import resolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
import replace from 'rollup-plugin-replace'
import copy from 'rollup-plugin-copy'
import babel from 'rollup-plugin-babel';
import pkg from './package.json';

const NODE_ENV = process.env.NODE_ENV || "development";
const outputFile = NODE_ENV === "production" ? "./dist/taste_ui.js" : "./dist/taste_ui_dev.js";

export default [
	// browser-friendly UMD build
	{
    input: 'src/main.js',
    output: {
			name: 'taste_ui',
			file: pkg.module,
      format: 'umd',
      globals: {
        react: 'React',
        'react-dom': 'ReactDOM',
        // 'next': 'next',
        'next/link': 'Link'
        // 'styled-jsx': 'style jsx'
      }
    },
    external: [
      'ms',
      'react',
      'react-dom',
      'next',
      'next/link',
      'styled-jsx',
      id => /^react|styled-jsx/.test(id)
    ],
    /* globals: {
      react: 'React',
      'react-dom': 'ReactDOM',
      'next': 'next',
      // 'next/link': 'Link',
      'styled-jsx': 'style'
    }, */
		plugins: [
      replace({
        "process.env.NODE_ENV": JSON.stringify(NODE_ENV)
      }),
      // copy({
      //   "src/assets": "dist/assets",
      //   verbose: true
      // }),
			// resolve(), // so Rollup can find `ms`
      babel({
        babelrc: false,
        presets: ['@babel/env', '@babel/preset-react'],
        exclude: 'node_modules/**'
      }),
      resolve(), // so Rollup can find `ms`
      commonjs() // so Rollup can convert `ms` to an ES module
    ]
	},

	// CommonJS (for Node) and ES module (for bundlers) build.
	// (We could have three entries in the configuration array
	// instead of two, but it's quicker to generate multiple
	// builds from a single configuration where possible, using
	// an array for the `output` option, where we can specify 
	// `file` and `format` for each target)
	/* {
		input: 'src/main.js',
		external: [
      'ms',
      'react',
      'react-dom'
    ],
    globals: {
      react: 'React',
      'react-dom': 'ReactDOM'
    },
    plugins: [
			resolve(), // so Rollup can find `ms`
      babel({
        babelrc: false,
        presets: ['@babel/env', '@babel/preset-react'],
        exclude: 'node_modules/**'
      }),
      commonjs() // so Rollup can convert `ms` to an ES module
    ],
		output: [
			{ file: pkg.main, format: 'cjs' },
			{ file: pkg.module, format: 'es' }
		]
	} */
];
