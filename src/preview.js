import React from "react";
import PropTypes from 'prop-types'

import Vars from '../src/consts/vars'
import Dummy from '../src/consts/dummy'

import TasteLoader from './elements/loader/taste_loader'
import TasteText from './elements/text/taste_text'
import TasteTitle from './elements/title/taste_title'
import TasteAnchor from './elements/anchor/taste_anchor'
import TasteButton from './elements/button/taste_button'
import TasteImage from './elements/image/taste_image'

import TasteHeader from './components/header/taste_header'
import TasteFooter from './components/footer/taste_footer'
import TasteGroupCard from './components/group_card/taste_group_card'
import TasteGroupDetail from './components/group_detail/taste_group_detail'
import TasteTopicCard from './components/topic_card/taste_topic_card'
import TasteTopicDetail from './components/topic_detail/taste_topic_detail'
import TasteUserCard from './components/user_card/taste_user_card'
import TasteTalkCard from './components/talk_card/taste_talk_card'
import TasteMainMenu from './components/main_menu/taste_main_menu'
import TastePagination from './components/pagination/taste_pagination'
import TasteInputText from './elements/input_text/taste_input_text'

import TasteNotificationCard from './components/notification_card/taste_notification_card'

const TasteUIContainer = () => (
  <div id="taste-ui-preview">
    <TasteHeader
      is_mobile={true}
      self={{id:1,username:'aku',picture:{small:'aku'}}}
      unread_notif={'0'}
      sso_host={''}
      home_host={''}
      reviews_host={''}
      image_host='/assets/images/'
    />
    <br/>
    <br/>
    <br/>
    <br/>
    <TasteTitle
      element_id={'main-title'}
      text={'List Title Type'}
      is_mobile={true}
    />
    {/* <TasteInputText
      is_mobile={true}
      value={props.new_topic.tags}
      placeholder="Topic tags here..."
    
    /> */}
    {/* <TastePagination/> */}
    {/* <TasteLoader
      is_mobile={true}
      text={'Loading'}
    /> */}
    <TasteTitle
      text={'TASTE_TITLE_TYPE.MAIN_TITLE'}
      type={Vars.TASTE_TITLE_TYPE.MAIN_TITLE}
      is_mobile={true}
    />
    <TasteTitle
      text={'TASTE_TITLE_TYPE.SUB_TITLE'}
      type={Vars.TASTE_TITLE_TYPE.SUB_TITLE}
      is_mobile={true}
    />
    <TasteTitle
      text={'TASTE_TITLE_TYPE.CARD_TITLE'}
      type={Vars.TASTE_TITLE_TYPE.CARD_TITLE}
      is_mobile={true}
    />
    <TasteTitle
      text={'TASTE_TITLE_TYPE.NAVIGATION'}
      type={Vars.TASTE_TITLE_TYPE.NAVIGATION}
      is_mobile={true}
    />
    <TasteTitle
      text={'TASTE_TITLE_TYPE.NAVIGATION_SELECTED'}
      type={Vars.TASTE_TITLE_TYPE.NAVIGATION_SELECTED}
      is_mobile={true}
    />
    <TasteTitle
      text={'CUSTOM'}
      type={'custom'}
      is_mobile={true}
    />
    <br/>

    <TasteMainMenu
      is_mobile={true}
      menus={Vars.MAIN_MENU}
    />
    <br/>
    
    <TasteText
      text={'List Text Type'}
      type={Vars.TASTE_TEXT_TYPE.TIME}
      is_mobile={true}
    />
    <TasteText
      text={'Vars.TASTE_TEXT_TYPE.DEFAULT'}
      is_mobile={true}
    />
    <TasteText
      text={'Vars.TASTE_TEXT_TYPE.DEFAULT_SPACING'}
      type={Vars.TASTE_TEXT_TYPE.DEFAULT_SPACING}
      is_mobile={true}
    />
    <TasteText
      text={'Vars.TASTE_TEXT_TYPE.DEFAULT_LIGHT'}
      type={Vars.TASTE_TEXT_TYPE.DEFAULT_LIGHT}
      is_mobile={true}
    />
    
    <TasteText
      text={'Vars.TASTE_TEXT_TYPE.DETAIL_NAME'}
      type={Vars.TASTE_TEXT_TYPE.DETAIL_NAME}
      is_mobile={true}
    />
    <TasteText
      text={'Vars.TASTE_TEXT_TYPE.DETAIL_INFO'}
      type={Vars.TASTE_TEXT_TYPE.DETAIL_INFO}
      is_mobile={true}
    />
    <TasteText
      text={'Vars.TASTE_TEXT_TYPE.TIME'}
      type={Vars.TASTE_TEXT_TYPE.TIME}
      is_mobile={true}
    />
    <TasteText
      text={'Vars.TASTE_TEXT_TYPE.MENTION'}
      type={Vars.TASTE_TEXT_TYPE.MENTION}
      is_mobile={true}
    />
    <TasteText
      text={'Vars.TASTE_TEXT_TYPE.INFOBAR_TOPIC'}
      type={Vars.TASTE_TEXT_TYPE.INFOBAR_TOPIC}
      is_mobile={true}
    />
    <TasteText
      text={'Vars.TASTE_TEXT_TYPE.INFOBAR_POST'}
      type={Vars.TASTE_TEXT_TYPE.INFOBAR_POST}
      is_mobile={true}
    />
    <TasteText
      text={'Vars.TASTE_TEXT_TYPE.SORT'}
      type={Vars.TASTE_TEXT_TYPE.SORT}
      is_mobile={true}
    />
    <TasteText
      text={'Vars.TASTE_TEXT_TYPE.PLACEHOLDER'}
      type={Vars.TASTE_TEXT_TYPE.PLACEHOLDER}
      is_mobile={true}
    />
    <TasteText
      text={'CUSTOM'}
      type={'custom'}
      style={'font-size:24px; color:#000;'}
      is_mobile={true}
    />
    
    <br/>
    <br/>
    <TasteButton
      text={'WHITE'}
      is_mobile={true}
      type={Vars.TASTE_BUTTON_TYPE.WHITE}
    />
    <br/>
    <TasteButton
      text={'WHITE ROUND'}
      is_mobile={true}
      type={Vars.TASTE_BUTTON_TYPE.WHITE_ROUND}
      style={'letter-spacing:5px;'}
      class_icon={'icon-ic_dob'}
    />

    <br/>
    <TasteButton
      text={'PINK'}
      is_mobile={true}
      type={Vars.TASTE_BUTTON_TYPE.PINK}
      class_icon={'icon-ic_facebook_white'}
    />
    <br/>
    <TasteButton
      text={'PINK ROUND'}
      is_mobile={true}
      type={Vars.TASTE_BUTTON_TYPE.PINK_ROUND}
    />
    <br/>
    <br/>
    
    {/* <TasteImage
      type={Vars.TASTE_IMAGE_TYPE.CIRCLE}
      style={'width:320px;'}
      is_mobile={true}
    /> */}
    {/* <TasteImage
      style={'width:240px;'}
      is_mobile={true}
    /> */}
    <div className='notif-card'>
    <TasteNotificationCard
      is_mobile={false}
      notification={Dummy.NOTIFICATION_CARD}
    />
    </div>
    {/* <TasteTopicCard
      is_mobile={true}
      topic={Dummy.TOPIC}
    /> */}
    {/* <TasteTalkCard
      is_mobile={false}
      is_title={true}
      talk={Dummy.TALK}
    /> */}
    <br/>
    <br/>
    {/* <TasteAnchor
      text={'this is Link >>'}
      url={'https://www.femaledaily.com'}
    /> */}
    <br/>
    <br/>
    <TasteFooter
      is_mobile={true}
      home_host={'lala'}
    />
   
    {/* <TasteGroupDetail
      is_mobile={true}
      group={Dummy.GROUP_DETAIL}
    /> */}

    {/* <TasteGroupCard
      is_mobile={true}
      group={Dummy.GROUP}
    /> */}

    



    <style>
      {`
        // #taste-ui-preview {
        //   margin: 0;
        //   padding: 0;
        //   background-color: #ffe;
        // }
        .notif-card {
          width: 320px;
        }
      `}
    </style>
  </div>
)

export default TasteUIContainer;