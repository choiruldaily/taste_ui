import React from "react";
import ReactDOM from "react-dom";

import Preview from './preview'
// this line is new
// we now have some nice styles on our react app
import "index.scss";

ReactDOM.render(
  <Preview/>,
  document.getElementById("root")
);