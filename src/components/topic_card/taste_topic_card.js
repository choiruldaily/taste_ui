import React from 'react'
import PropTypes from 'prop-types'

import { moment_date } from '../../helpers/utils'

import TasteTopicCardMobile from './taste_topic_card_mobile'

const TasteTopicCard = ({ is_mobile, is_user_card, topic, host, topics_asset_endpoint, profile_image_endpoint, handle_modal }) => {
  const props = { 
    is_mobile               : is_mobile, 
    is_user_card            : is_user_card || false, 
    topic                   : topic || {},
    images                  : topic.images || [],
    video                   : topic.videos && topic.videos.length>0 ? topic.videos[0] : [],
    topic_date              : topic && topic.created_at ? moment_date({ date: topic.created_at }) : '',
    host                    : host || '',
    topics_asset_endpoint   : topics_asset_endpoint || '',
    profile_image_endpoint  : profile_image_endpoint || '',
    url: {
      pathname  : '/topic',
      query     : { topic: topic.title_slug, order: 'newest', page: 1 },
      as        : `/topics/${topic.title_slug}?order=newest&page=1`
    },

    handle_modal
  }
  
  if (is_mobile) {
    return (
      <TasteTopicCardMobile
        { ...props }
      />
    )
  } else {
    return (
      <TasteTopicCardMobile
        { ...props }
      />
    )
  }
}

TasteTopicCard.propTypes = {
  is_mobile   : PropTypes.bool.isRequired,
  topic       : PropTypes.object.isRequired
}

export default TasteTopicCard

/**
 * topic
 * - id
 * - name
 * - slug
 * - info: {}
 * - user: {}
 */