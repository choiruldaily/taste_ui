import React from 'react'
import Link from 'next/link'
import Vars from '../../consts/vars'

import TasteImage from '../../elements/image/taste_image'
import TasteVideo from '../../elements/video/taste_video'
import TasteTitle from '../../elements/title/taste_title'
import TasteText from '../../elements/text/taste_text'
import TasteInfoIcon from '../../elements/info_icon/taste_info_icon'
import TasteTag from '../../elements/tag/taste_tag'
import TasteUserCard from '../../components/user_card/taste_user_card'

const TasteTopicCardMobile = (props) => {
  return (
    <div className='taste-topic-card'>
      <div className='topic-user'>
        <TasteText
          is_mobile={props.is_mobile}
          text={props.topic_date}
          type={Vars.TASTE_TEXT_TYPE.TIME}
          style={'text-align:right;margin-bottom:-20px;'}
        />
        {
          props.is_user_card?
          <TasteUserCard
            is_mobile={props.is_mobile}
            user={props.topic.user}
            image_endpoint={props.profile_image_endpoint}
          />
          :
            null
        }
        
      </div>
      
      {
        props.host?
          <Link href={`${props.host}${props.url.as}`}>
            <a>
              <TasteTitle
                is_mobile={props.is_mobile}
                text={props.topic.title}
                type={Vars.TASTE_TITLE_TYPE.DEFAULT}
                additional_class={'topic-title'}
              />
            </a>
          </Link>
        : 
          <Link 
            href={{
              pathname  : props.url.pathname,
              query     : props.url.query
            }}
            as={props.url.as}
          >
            <a>
              <TasteTitle
                is_mobile={props.is_mobile}
                text={props.topic.title}
                type={Vars.TASTE_TITLE_TYPE.DEFAULT}
                additional_class={'topic-title'}
              />
            </a>
          </Link>
      }
      
      <div className="taste-topic-card-info">

        <div className='info-icon-topic'>
          <span className="icon-icon_member ict-member">
            <span className="path1"></span><span className="path2"></span>
          </span>
          <TasteText
            text={props.topic.total_users||'0'}
            is_mobile={true}
            type={Vars.TASTE_TEXT_TYPE.INFOBAR_TOPIC}
          />
        </div>

        <div className='info-icon-topic'>
          <span className="icon-icon_post ict-post">
          </span>
          <TasteText
            text={props.topic.total_talks||'0'}
            is_mobile={true}
            type={Vars.TASTE_TEXT_TYPE.INFOBAR_TOPIC}
          />
        </div>
      </div>

      <TasteText
        is_mobile={props.is_mobile}
        text={props.topic.description || ''}
      />

      <div className='topic-images'>
        {
          props.images.length>3?
            props.images.map((item,idx)=>
              idx<2?
                <TasteImage
                  key={idx}
                  is_mobile={props.is_mobile}
                  url={`${props.topics_asset_endpoint}/${item}`}
                  type={Vars.TASTE_IMAGE_TYPE.CLICKABLE}
                  style={'border-radius: 4px;width:85px; height:85px; margin-right:10px; justify-content: center; align-items: center; overflow: hidden;'}

                  additional_class={'list-topic'}
                  on_click={props.handle_modal}
                />
              : idx<3?
                <TasteImage
                  key={idx}
                  is_mobile={props.is_mobile}
                  text={props.images.length-3}
                  type={Vars.TASTE_IMAGE_TYPE.TEXT}
                  url={`${props.topics_asset_endpoint}/${item}`}
                  style={'width:85px; height:85px; margin-right:10px;'}
                  
                />
              : null
            )
          :
            props.images.map((item,idx)=>
              <TasteImage
                key={idx}
                is_mobile={props.is_mobile}
                url={`${props.topics_asset_endpoint}/${item}`}
                type={Vars.TASTE_IMAGE_TYPE.CLICKABLE}
                style={'border-radius: 4px;width:85px; height:85px; margin-right:10px; justify-content: center; align-items: center; overflow: hidden;'}

                additional_class={'list-topic'}
                on_click={props.handle_modal}
              />
            )
        }
      </div>

      {
        props.video.thumbnail?
          <div className='topic-videos'>
            <TasteImage
              is_mobile={props.is_mobile}
              type={Vars.TASTE_IMAGE_TYPE.CLICK_VIDEO}
              url={`${props.topics_asset_endpoint}/${props.video.thumbnail}`}
              url_video={`${props.topics_asset_endpoint}/${props.video.video}`}
              style={'border-radius: 4px;width:85px; height:85px; margin-right:10px; justify-content: center; align-items: center; overflow: hidden;'}
              additional_class={'video-topic'}

              on_click={props.handle_modal}
            />
          </div>
        :
          null
      }

      <style>{`
        .ic-play {
          position: absolute;
          z-index: 1;
          font-size: 32px;
        }
        .sample {
          flex-shrink: 0;
          display: flex;
          background-color: #fff;
          position: relative;
          justify-content: center;
          align-items: center;
        }
        .ict-member {
          font-size: 18px;
          margin: 0 10px 0 0;
        }
        .ict-post {
          font-size: 16px;
          margin: 0 10px 0 20px;
        }
        .info-icon-topic {
          display: flex;
          align-items: center;
          flex-flow: row;
          margin: 5px 0;
        }
        .tags {
          width: 100%;
          display: flex;
          align-items: center;
          flex-flow: wrap;
        }
        .topic-images {
          width: 100%;
          display: flex;
          flex-flow: row;
          margin: 20px 0 10px 0;
        }
        .topic-images:empty {
          display: none;
        }
        .topic-videos {
          width: 100%;
          display: flex;
          flex-flow: row;
          margin: 20px 0 10px 0;
        }
        .topic-videos:empty {
          display: none;
        }
        .full {

        }
        .taste-topic-card {
          display: flex;
          flex-flow: column;
          width: 320px;
          border: 1px solid #efefef;
          padding: 20px;
          border-radius: 8px;
          box-shadow: -3px 4px 10px 0 rgba(0, 0, 0, 0.08);
          background-color: #ffffff;
          word-break: break-word;

        }
        .taste-topic-card-info {
          display: flex;
          flex-flow: wrap;
          width: 100%;
          margin: 15px 0;
        }
        .topic-user {
          display: flex;
          flex-flow: column;
          width: 100%;
          margin: 0 0 20px 0;
        }
      `}</style>
    </div>
  )
}

export default TasteTopicCardMobile