import React from 'react'
import Link from 'next/link'

const TasteHeaderMobile = (props) => {
  return (
    <div className="mobile-header">
      <div className="mobile-globalheader">
        <div className="gbheader-left">
        <i className="icon-ic_menu2 ic-menu"></i>
          <Link href='/'>
            <a className="icon-logo_fd ic-fd"></a>
          </Link>
        </div>

        <div className="gbheader-right">

          {
            props.self.id?
              <div className="gbheader-userprofile">
                <div className="gbheader-userimage-cover">
                  <img className="gbheader-userimage" src={props.image_size(props.image_src({ image: props.self.picture.small, endpoint: props.image_host }),42)} alt="" />
                </div>
                <h3>hi!</h3>
                <div className="gbheader-dropdown-profile-notif">
                  <Link href={`${props.reviews_host}/notification`}><a className="gbheader-unread-notif">{props.unread_notif} Unread Notification <span>&gt;</span></a></Link>
                  <Link href={{ pathname: `${props.reviews_host}/user/feeds`, query: { user: props.self.username }}} as={`${props.reviews_host}/user/${props.self.username}/feeds`}><a className="gbheader-read-notif" >Feed</a></Link>
                  <Link href={{ pathname: `${props.reviews_host}/user/profile`, query: { user: props.self.username, tab: 'reviews' }}} as={`${props.reviews_host}/user/${props.self.username}?tab=reviews`}><a className="gbheader-read-notif" >My Profile</a></Link>
                  <Link href={{ pathname: `${props.reviews_host}/user/my_account`, query: { user: props.self.username, tab: 'account_information' }}} as={`${props.reviews_host}/my/account?tab=account_information`}><a className="gbheader-read-notif" >Settings</a></Link>
                  <Link href={props.sso_host+'/logout'}><a className="gbheader-read-notif">Logout</a></Link>
                </div>
              </div>
            :
              <div className="gbheader-login">
                <a href={ props.sso_host }>LOGIN</a>
              </div>
          }
        </div>
      </div>

      <form className="gtmenu-search-input" onSubmit={props.submit_search}>
        <i className="icon-ic_search_black ic-search"></i>
        <input className="gtmenu-search-input-field" type="text" placeholder="Search products, articles, topics, brands, etc" defaultValue={props.search_text} onKeyUp={props.update_search}/>
      </form>

      <style>{`
        .mobile-header {
          top: 0;
          background-color: #fff;
          position: fixed;
          z-index: 2;
          width: 100%;
        }
        .mobile-globalheader {
          width: 100%;
          height: 60px;
          display: flex;
          flex-flow: row;
          justify-content: space-between;
          align-items: center;
          border-bottom:1px solid lightgray;
          flex-shrink: 0;
        }
        .gbheader-left {
          display: flex;
          flex-flow: row;
          align-items: center;

        }
        .gbheader-right {
          display: flex;
          flex-flow: row;    
        }
        .gtmenu-search-input-field {
          box-sizing: border-box;
          width: 100%;
          height: 32px;
          border-radius: 4px;
          font-family: Noto Serif;
          font-size: 12px;
          outline: none;
          border: none;
          background: none;
        }
        .gbheader-menu {
          display: flex;
          flex-flow: row;
          align-items: center;
        }
        .gbheader-menutxt {
          font-family: Montserrat;
          font-weight: bold;
          font-size: 14px;
          word-spacing: 1px;
          text-decoration: none;
          color: #000;
          letter-spacing: 1.5px;
          font-size: 11px;
          margin-left: 5px;
        }
        .gbheader-menu-active {
          color: #db284e;
          padding: 8px 0;
          background: /static/images/img_menu_active.png);
          background-position: 2px 2px;
          background-repeat: no-repeat;
        }

        .mobile-globalheader-menu {
          width: 100%;
          display: flex;
          flex-flow: row;
          justify-content: space-between;
          align-items: center;
        }

        .mobile-globalheader-profile-container {
          border-left: 1px solid #e0e0e0;
          height: 60px;
          width: 60px;
          display: flex;
          flex-flow: row;
          justify-content: center;
          align-items: center;
        }

        .mobile-globalheader-profile-container h3{
          font-size: 11px;
          color: #db284e;
          font-family: Montserrat;
          font-weight: bold;
          text-transform: capitalize;
          margin: 0 0 0 5px;
        }

        .mobile-globalheader-image-profile {
          width: 30px;
          height: 30px;
        }

        .gbheader-userprofile {
          border-left: 1px solid #e0e0e0;
          background-color: #FFF;
          display: flex;
          justify-content: space-between;
          align-items: center;
          padding: 0px 10px;
          height: 59px;
          cursor: pointer;
        }
        .gbheader-userprofile h3 {
          font-size: 11px;
          color: #db284e;
          font-family: Montserrat;
          font-weight: bold;
          text-transform: capitalize;
          margin: 0 0 0 5px;
        }
        .gbheader-userimage-cover {
          display: flex;
          align-items: center;
          justify-content: center;
          width: 25px;
          height: 25px;
          overflow: hidden;
          border-radius: 50%;
        }
        .gbheader-userimage {
          width: 100%;
        }
        .gbheader-username {
          font-family: Montserrat;
          font-weight: lighter;
          font-size: 16px;
          color: #fff;
        }
        .gbheader-dropdown-profile-notif {
          right: 0;
          font-size: 15px;
          display: none;
          position: absolute;
          background-color: #f9f9f9;
          width: 100%;
          top: 60px;
          box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
          z-index: 3;
        }
        .gbheader-userprofile:hover .gbheader-dropdown-profile-notif {
          display: block;
        }
        .gbheader-read-notif {
          font-family: Montserrat;
          font-weight: normal;
          font-size: 14px;
          color: #4A4A4A;
          padding: 10px 10px;
          display: block;
          text-decoration:none
        }
        .gbheader-unread-notif {
          font-family: Montserrat;
          font-weight: normal;
          font-size: 14px;
          border-top: 1px solid #FFF;
          background-color: #DB284E;
          color: #fff;
          padding: 10px 10px;
          text-decoration: none;
          display: flex;
          justify-content: space-between;
        }

        .gbheader-login {
          border-left: 1px solid #e0e0e0;
          background-color: #fff;
          display: flex;
          justify-content: center;
          align-items: center;
          height: 59px;
          cursor: pointer;
        }
        .gbheader-login a{
          font-size: 11px;
          color: #db284e;
          font-family: Montserrat;
          font-weight: bold;
          text-transform: capitalize;
          padding: 0 10px;
        }

        .gtmenu-search-input {
          background-color: #f7f7f7;
          display: flex;
          align-items: center;
        }
        .ic-search {
          margin: 0px 10px;
          font-size: 20px;
        }
        .ic-menu {
          font-size: 24px;
          margin-right: 10px;
        }
        .ic-fd {
          font-size: 32px;
        }  
      `}</style>
    </div>
  )
}

export default TasteHeaderMobile