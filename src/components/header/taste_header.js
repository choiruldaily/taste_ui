import React from 'react'
import PropTypes from 'prop-types'

import Vars from '../../consts/vars'

import TasteHeaderMobile from './taste_header_mobile'

const TasteHeader = ({ 
  is_mobile, 
  self, 
  unread_notif, 
  sso_host, 
  home_host, 
  reviews_host,
  image_host,
  font_host,

  ga_host_id,
  ga_general_id,
  gtm_id,
  fb_app_id
}) => {
  const props = { 
    self          : self || {},
    search_text   : '',
    unread_notif  : unread_notif || '0', 
    sso_host      : sso_host, 
    home_host     : home_host, 
    reviews_host  : reviews_host,
    image_host    : image_host || Vars.IMAGE_ENDPOINT,
    font_host     : font_host || '/assets/fonts/',

    noscript_fb   : noscript_fb,
    noscript_gtm  : noscript_gtm,
    image_src     : image_src,
    image_size    : image_size,

    update_search,
    submit_search
  }

  // Add global script
  if (typeof window !== 'undefined') {
    if (ga_host_id || ga_general_id) {
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
      
      if (ga_host_id) {
        ga('create', ga_host_id, 'auto');
        ga('send', 'pageview');
      }

      if (ga_host_id) {
        ga('create', ga_general_id, 'auto', 'fdn');
        ga('fdn.send', 'pageview');
      }
    }
    
    {/* Google Tag Manager */}
    if (gtm_id) {
      (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
      new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
      j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
      })(window,document,'script','dataLayer',gtm_id);
      {/* End of Google Tag Manager */}
    }

    // Facebook pixel
    if (fb_app_id) {
      if(typeof fbq === 'undefined') {
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
          n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
          n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
          t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
          document,'script','https://connect.facebook.net/en_US/fbevents.js');
        
          fbq('init', fb_app_id);          
          fbq('track', "PageView");
      } else {
        fbq('track', "PageView");
      }

      (function(d, s, id) {
        // Load FB-SDK
        var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/en_US/sdk.js";
          fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'))
  
      // Init FB-SDK
      /* window.fbAsyncInit = function () {
        FB.init({
          appId      : fb_app_id,   //'1732196183480267',
          cookie     : true,  // enable cookies to allow the server to access the session
          xfbml      : true,  // parse social plugins on this page
          version    : 'v2.11' // use graph api version 2.8
        })
        FB.AppEvents.logPageView();
        FB.Event.subscribe('auth.statusChange', function (res) {
          if (res.authResponse) {
            this.fb_get_status()
          } else {
            // console.log('---->User cancelled login or did not fully authorize.');
          }
        }.bind(this))
      }.bind(this) */
    }

    // New script
    window['_fs_debug'] = false; window['_fs_host'] = 'fullstory.com'; window['_fs_org'] = '9YKWS'; window['_fs_namespace'] = 'FS'; (function(m,n,e,t,l,o,g,y){ if (e in m) {if(m.console && m.console.log) { m.console.log('FullStory namespace conflict. Please set window["_fs_namespace"].');} return;} g=m[e]=function(a,b){g.q?g.q.push([a,b]):g._api(a,b);};g.q=[]; o=n.createElement(t);o.async=1;o.src='https://'+_fs_host+'/s/fs.js'; y=n.getElementsByTagName(t)[0];y.parentNode.insertBefore(o,y); g.identify=function(i,v){g(l,{uid:i});if(v)g(l,v)};g.setUserVars=function(v){g(l,v)}; y="rec";g.shutdown=function(i,v){g(y,!1)};g.restart=function(i,v){g(y,!0)}; g.identifyAccount=function(i,v){o='account';v=v||{};v.acctId=i;g(o,v)}; g.clearUserCookie=function(c,d,i){if(!c || document.cookie.match('fs_uid=[`;`]*`[`;`]*`[`;`]*`')){ d=n.domain;while(1){n.cookie='fs_uid=;domain='+d+ ';path=/;expires='+new Date(0).toUTCString();i=d.indexOf('.');if(i<0)break;d=d.slice(i+1)}}}; })(window,document,window['_fs_namespace'],'script','user');

    window.googletag = window.googletag || {};
    var googletag = window.googletag;
    googletag.cmd = googletag.cmd || [];
    (function() {
      var gads = document.createElement('script');
      gads.async = true;
      gads.type = 'text/javascript';
      var useSSL = 'https:' == document.location.protocol;
      gads.src = (useSSL ? 'https:' : 'http:') +
        '//www.googletagservices.com/tag/js/gpt.js';
      var node = document.getElementsByTagName('script')[0];
      node.parentNode.insertBefore(gads, node);
    })();
  }
  // End of add global script

  if (is_mobile) {
    return (
      <TasteHeaderMobile
        { ...props }
      />
    )
  } else {
    return (
      <TasteHeaderMobile
        { ...props }
      />
    )
  }

  function update_search (e) {
    if (e.keyCode === 13) {
      submit_search()
    } else {
      props.search_text = e.target.value
    }
    
  }

  function submit_search () {
    if (window.location.href.includes('review.') || window.location.href.includes('reviews.')) {
      window.location.href = `/search?q=${props.search_text}&tab=all`
    } else {
      window.location.href = `${reviews_host}/search?q=${props.search_text}&tab=all`
    }
  }
}


TasteHeader.propTypes = {
  is_mobile     : PropTypes.bool.isRequired,
  self          : PropTypes.object.isRequired,
  unread_notif  : PropTypes.string.isRequired,
  home_host     : PropTypes.string.isRequired
  // image_hos  t  : PropTypes.string.isRequired
}


/* const update_search = (e) => {
	console.log('TCL: update_search -> e', e)
  //
} */

const submit_search = (e) => {
  //
}

const noscript_fb = () => {
  return (
    <NoScript>
      {/* Facebook pixel */}
      <img height="1" width="1" style={{display:'none'}} src="https://www.facebook.com/tr?id=134434910307423&ev=PageView&noscript=1"/>
      {/* End of Facebook pixel */}
    </NoScript>
  )
}

const noscript_gtm = () => {
  return (
    <NoScript>
      {/* Google Tag Manager */}
      <iframe src={`//www.googletagmanager.com/ns.html?id=${Env.GTM_APP_ID}`} height="0" width="0" style={{display:'none',visibility:'hidden'}}></iframe>
      {/* End Google Tag Manager */}
    </NoScript>
  )
}

const image_src = ({image, endpoint}) => {
  return image? (image.split('://').length>1?'':endpoint)+image : Vars.DEFAULT_IMAGE
}

const image_size = (url,width) => {
  if (url.constructor === Object) {
    return 'http://static.femaledaily.com/dyn/'+width+'/images'+url.xs.split('images')[1]
  } else {
    return 'http://static.femaledaily.com/dyn/'+width+'/images'+url.split('images')[1]
  }
}

export default TasteHeader

/**
 * user
 * - self
 * - setting
 * search
 */