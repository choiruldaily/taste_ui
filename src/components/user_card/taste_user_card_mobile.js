import React from 'react'
import Link from 'next/link'

import Vars from '../../consts/vars'

import TasteText from '../../elements/text/taste_text'
import TasteImage from '../../elements/image/taste_image'

const TasteUserCardMobile = (props) => {
  return (
    <div className='taste-user-card'>
      <div className='user-image'>
        <TasteImage
          is_mobile={props.is_mobile}
          url={props.image_url}
          type={Vars.TASTE_IMAGE_TYPE.CIRCLE}
          additional_class={'user-card'}
        />
      </div>
      <div className='user-info'>
        <TasteText
          is_mobile={props.is_mobile}
          text={props.username}
          type={Vars.TASTE_TEXT_TYPE.DETAIL_NAME}
          style={'margin: 0 0 5px 0;'}
          additional_class={'user-card'}
        />

        <TasteText
          is_mobile={props.is_mobile}
          text={props.user_info}
          type={Vars.TASTE_TEXT_TYPE.DETAIL_INFO}
        />
      </div>
      <style>{`
        .taste-user-card {
          display: flex;
          flex-flow: row;
          align-items: center;
        }
        .user-image {
          width: 30px;
          height: 30px;
          margin: 0 15px 0 0;
          flex-shrink: 0;
        }
        .user-info {

        }
        
      `}</style>
    </div>
  )
}

export default TasteUserCardMobile