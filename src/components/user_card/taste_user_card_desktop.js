import React from 'react'
import Link from 'next/link'

import Vars from '../../consts/vars'

import TasteText from '../../elements/text/taste_text'
import TasteImage from '../../elements/image/taste_image'

const TasteUserCardDesktop = (props) => {
  return (
    <div className='taste-user-card'>
      <div className='user-card-desktop-image'>
        <TasteImage
          is_mobile={props.is_mobile}
          url={props.image_url}
          type={Vars.TASTE_IMAGE_TYPE.CIRCLE}
          additional_class={'user-card'}
        />
      </div>
      <div className='user-card-desktop-text'>
        <TasteText
          is_mobile={false}
          text={props.username}
          type={Vars.TASTE_TEXT_TYPE.DETAIL_NAME}
          additional_class={'user-card-desktop-name'}
        />
      </div>
      <div className='user-card-desktop-text'>
        <TasteText
          is_mobile={props.is_mobile}
          text={props.user.level}
          type={Vars.TASTE_TEXT_TYPE.DETAIL_INFO}
        />
      </div>
      <div className='user-card-desktop-text'>
        <TasteText
          is_mobile={props.is_mobile}
          text={props.user.age_range}
          type={Vars.TASTE_TEXT_TYPE.DETAIL_INFO}
        />
      </div>
      <style>{`
        .taste-user-card {
          display: flex;
          flex-flow: column;
        }
        .user-card-desktop-image {
          width: 52px;
          height: 52px;
          flex-shrink: 0;
          margin: 0 0 15px 0;
        }
        .user-card-desktop-text {
          margin: 0 0 5px 0;
        }
        
      `}</style>
    </div>
  )
}

export default TasteUserCardDesktop