import React from 'react'
import PropTypes from 'prop-types'

import TasteUserCardMobile from './taste_user_card_mobile'
import TasteUserCardDesktop from './taste_user_card_desktop'

const TasteUserCard = ({ is_mobile, user={}, image_endpoint }) => {
  const props = { 
                  user        : user,
                  is_mobile   : is_mobile,
                  username    : user.id ? user.username || '-' : '-',
                  user_info   : user.id ? user.level+', '+user.age_range : '-',
                  image_url   : user.user_pics ? (user.user_pics[0].usrpic_userpics_large.split('://').length>1 ? '': image_endpoint)+user.user_pics[0].usrpic_userpics_large : 'http://pauillac.inria.fr/~harley/pics/square.gif'
                }
  
  if (is_mobile) {
    return (
      <TasteUserCardMobile
        { ...props }
      />
    )
  } else {
    return (
      <TasteUserCardDesktop
        { ...props }
      />
    )
  }
}

TasteUserCard.propTypes = {
  is_mobile   : PropTypes.bool.isRequired,
  user        : PropTypes.object.isRequired,
  // home_endpoint   : PropTypes.string.isRequired
}

export default TasteUserCard