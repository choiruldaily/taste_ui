import React from 'react'
import Link from 'next/link'

import Vars from '../../consts/vars'

import TasteImage from '../../elements/image/taste_image'
import TasteTitle from '../../elements/title/taste_title'
import TasteText from '../../elements/text/taste_text'
import TasteInfoIcon from '../../elements/info_icon/taste_info_icon'
import TasteTag from '../../elements/tag/taste_tag'
import TasteButton from '../../elements/button/taste_button'

const TasteGroupDetailMobile = (props) => {
  return (
    <div className='taste-group-detail'>
      <div className='taste-group-top'>
        <TasteImage
          is_mobile={props.is_mobile}
          url={props.image_url}
          // url='http://pauillac.inria.fr/~harley/pics/square.gif'
          style={'width:100px;height:100px;'}
          type= {Vars.TASTE_IMAGE_TYPE.CIRCLE}
          additional_class={'group-detail'}
        />

        <div className="taste-group-detail-info">
          <div className='info-icon-group'>
            <span className="icon-icon_member ic-member">
              <span className="path1"></span><span className="path2"></span>
            </span>
            <TasteText
              text={props.group.total_subscribers||'-'}
              is_mobile={true}
              // type={Vars.TASTE_TEXT_TYPE.DETAIL_NAME}
            />
          </div>

          <div className='info-icon-group'>
            <span className="icon-icon_topic ic-topic">
            </span>
            <TasteText
              text={props.group.total_topics||'-'}
              is_mobile={true}
              // type={Vars.TASTE_TEXT_TYPE.DETAIL_NAME}
            />
          </div>

          <div className='info-icon-group'>
            <span className="icon-icon_post ic-post">
            </span>
            <TasteText
              text={props.group.total_talks||'-'}
              is_mobile={true}
              // type={Vars.TASTE_TEXT_TYPE.DETAIL_NAME}
            />
          </div>
        </div>
      </div>


        <TasteTitle
          is_mobile={props.is_mobile}
          type={Vars.TASTE_TITLE_TYPE.MAIN_TITLE}
          text={props.group.title || 'Taste Title'}
          style={'margin: 20px 0 0 0;'}
        />

        <TasteText
          is_mobile={props.is_mobile}
          text={props.group.description || 'Placeholder description'}
          type={Vars.TASTE_TEXT_TYPE.DEFAULT}
          style={'text-align:center; margin:10px 5px;'}
          additional_class={'group-desc'}
        />


        <div className='tags'>
          <TasteText
            is_mobile={props.is_mobile}
            text={'Tags'}
            type={Vars.TASTE_TEXT_TYPE.INFOBAR_TOPIC}
          />
          {
            props.group.tags && props.group.tags.map((item,idx)=>
              <TasteTag
                key={idx}
                is_mobile={props.is_mobile}
                text={item}
              />
            )
          }
        </div>
        <div className='button-contain'>
          {
            props.group.is_join?
            <TasteButton
              is_mobile={props.is_mobile}
              type={Vars.TASTE_BUTTON_TYPE.WHITE_ROUND}
              text={props.button_text}
              class_icon={'icon-icon_check'}
              style={'margin-top:auto; width:inherit; justify-content:center; letter-spacing:5px;'}
              style_icon={'font-size: 12px;margin: 2px 2px 0 0;'}
              on_click={props.handle_join}
              additional_class={'member'}
            />
          :
            <TasteButton
              is_mobile={props.is_mobile}
              type={Vars.TASTE_BUTTON_TYPE.PINK_ROUND}
              text={props.button_text}
              style={'margin-top:auto; width:inherit; justify-content:center; letter-spacing:5px;'}

              on_click={props.handle_join}
            />
          }
        </div>

      <style>{`
        .ic-post {
          font-size: 16px;
          margin: 0 10px 0 0;
        }
        .ic-topic {
          font-size: 14px;
          margin: 0 10px 0 0;
        }
        .ic-member {
          font-size: 18px;
          margin: 0 10px 0 0;
        }
        .taste-group-detail {
          display: flex;
          flex-flow: column;
          width: 280px;
          align-items: center;
          background-color: #fff;
        }
        .group-icon {
          margin: 0 0 15px 0;
        }
        .button-contain {
          padding: 0 15px;
          margin: 20px 0;

          width: 100%;
        }
        .taste-group-top {
          display: flex;
          flex-flow: row;
          align-items: center;
        }
        .taste-group-detail-info {
          display: flex;
          flex-flow: column;
          margin-left: 30px;
        }
        .tags {
          width: 100%;
          display: flex;
          align-items: center;
          flex-flow: wrap;
        }
        .info-icon-group {
          display: flex;
          align-items: center;
          flex-flow: row;
          margin: 5px 0;
        }
        
      `}</style>
    </div>
  )
}

export default TasteGroupDetailMobile