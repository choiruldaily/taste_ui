import React from 'react'
import PropTypes from 'prop-types'

import TasteGroupDetailMobile from './taste_group_detail_mobile'

const TasteGroupDetail = ({ is_mobile, group, groups_asset_endpoint, on_join }) => {
  const props = { 
    is_mobile     : is_mobile || true, 
    group         : group || {}, 
    image_url     : `${groups_asset_endpoint}/${group.group_picture}`,
    button_text   : group.is_join ? 'MEMBER' : '+ JOIN',

    handle_join
  }
  
  if (is_mobile) {
    return (
      <TasteGroupDetailMobile
        { ...props }
      />
    )
  } else {
    return (
      <TasteGroupDetailMobile
        { ...props }
      />
    )
  }
  function handle_join() {
    on_join(group)
  }
}

TasteGroupDetail.propTypes = {
  is_mobile   : PropTypes.bool.isRequired,
  group       : PropTypes.object.isRequired
}

export default TasteGroupDetail