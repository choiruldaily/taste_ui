import React from 'react'
import PropTypes from 'prop-types'

import TasteFooterMobile from './taste_footer_mobile'

const TasteFooter = ({ is_mobile=false, home_host, image_host='/assets/images/' }) => {
  const props = { home_host, image_host }
  
  if (is_mobile) {
    return (
      <TasteFooterMobile
        { ...props }
      />
    )
  } else {
    return (
      <TasteFooterMobile
        { ...props }
      />
    )
  }
}

TasteFooter.propTypes = {
  is_mobile   : PropTypes.bool.isRequired,
  home_host   : PropTypes.string.isRequired
  // image_host  : PropTypes.string.isRequired
}

export default TasteFooter