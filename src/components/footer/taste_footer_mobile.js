import React from 'react'
import PropTypes from 'prop-types'
import Link from 'next/link'

const TasteFooterMobile = ({ home_host, image_host }) => (
  <div className="footer-taste">
    <ul className="footer-link-list">
      <li><a href={`${home_host}/about`} target="_self">About Us</a></li>
      <li><a href={`${home_host}/feedback`}>Feedback</a></li>
      <li><a href="http://editorial.femaledaily.com/contact">Contact</a></li>
    </ul>
    <ul className="footer-link-list">
      <li><a href={`${home_host}/terms-and-conditions`}>Terms &amp; Conditions</a></li>
      <li><a href={`${home_host}/privacy-policy`} target="_self">Privacy Policy</a></li>
      <li><a href={`${home_host}/help`} target="_self">Help</a></li>
    </ul>
    <ul className="footer-link-list">
      <li><a href="http://awards.femaledaily.com/">Awards</a></li>
      {/* <li><a href="http://femaledaily.com/blog/category/female-daily-network">Career</a></li> */}
      <li><a href="http://editorial.femaledaily.com/newsletter">Newsletter</a></li>
    </ul>
    <p className="footer-text">Download Our Mobile App</p>
    <div className="footer-store">
      <Link href='https://itunes.apple.com/id/app/female-daily-beauty-review/id1160742672?l=id&mt=8'>
        <a>
        <img src={image_host+"btn_appstore.png"}/>
        </a>
      </Link>
      <Link href='https://play.google.com/store/apps/details?id=com.fdbr.android&hl=en_GB'>
        <a>
          <img src={image_host+"btn_playstore.png"}/>
        </a>
      </Link>
    </div>
    <div className="footer-logo" >
      <Link href={home_host} >
        <a className="icon-logo_fd ic-fd"/>
      </Link>
    </div>
    <p className="footer-header1">Copyright &copy; 2015 - 2018<br/>Female Daily Network &#8729; All the rights reserved</p>
    <ul className="footer-sosmed-list">
      <li><a className="icon-ic_facebook fs25" href="https://www.facebook.com/FemaleDailyNetwork/"/></li>
      <li><a className="icon-ic_twitter fs25" href="https://twitter.com/femaledaily"/></li>
      <li><a className="icon-ic_instagram fs25" href="https://www.instagram.com/femaledailynetwork/"/></li>
      <li><a className="icon-ic_youtube fs25" href="https://www.youtube.com/user/FemaleDailyNetwork"/></li>
    </ul>

    <style>
      {`
        ul {
          margin: 0;
          padding: 0;
        }
        .ic-fd {
          font-size: 32px;
        }
        .footer-taste {
          padding: 20px 20px 50px 20px;
          border-top: 2px solid #e0e0e0;
          box-sizing: border-box;
          width: 100%;
        }
        .footer-text {
          font-family: Montserrat;
          font-size: 12px;
          line-height: 1.5;
          text-align: center;
          color: #db284e;
          margin-top: 30px;
        }
        .footer-link-list {
          display: flex;
          justify-content: center;
          flex-flow: row;
          flex-shrink: 0;
          margin: 0 0 10px 0;
        }
        li{
          list-style-type: none;
        }
        .footer-link-list li a{
          font-size: 11px;
          font-family: Montserrat;
          font-weight: bold;
          text-transform: capitalize;
          padding: 10px;
          color: #000;
          text-decoration:none
        }
        .footer-logo {
          width: 100%;
          display: flex;
          justify-content: center;
          align-items: center;
          flex-shrink: 0;
          margin: 15px 0;
        }
        
        .footer-header1 {
          font-family: Montserrat;
          font-size: 12px;
          line-height: 1.5;
          text-align: center;
          color: #4a4a4a;
        }
        .footer-sosmed-list {
          display: flex;
          flex-flow: row;
          width: 100%;
          justify-content: center;
          padding: 0;
        }
        .footer-sosmed-list li{
          margin: 0;
          padding: 5px;
        }
        .footer-store {
          display: flex;
          justify-content: space-between;
          margin-bottom: 40px;
          width: 280px;
          margin: 0 auto;
        }
        
      `}
    </style>
  </div>
)

TasteFooterMobile.propTypes = {
  home_host: PropTypes.string.isRequired
}

export default TasteFooterMobile