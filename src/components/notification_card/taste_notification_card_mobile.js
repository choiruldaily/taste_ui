import React from 'react'
import Link from 'next/link'

import Vars from '../../consts/vars'

import TasteText from '../../elements/text/taste_text'
import TasteImage from '../../elements/image/taste_image'

const TasteNotificationCardMobile = (props) => {
  return (
    <Link href="/">
      <a className='notification-card'>
        <div className='notification-user'>
          <div className='user-image'>
            <TasteImage
              is_mobile={props.is_mobile}
              url={props.image_url}
              type={Vars.TASTE_IMAGE_TYPE.CIRCLE}
              additional_class={'user-notification'}
            />
          </div>
          <div className='user-info'>
            <p className='notification-text'>
              <span className='notification-username'>{props.username} </span>
            {
              props.notification.type===Vars.NOTIFICATIONS_TYPE.NEW_TALK?
                <span className='notification-type'>add talk in your topic</span>
              :
              props.notification.type===Vars.NOTIFICATIONS_TYPE.REPLY?
                <span className='notification-type'>replied your talk</span>
              :
                <span className='notification-type'>liked your talk</span>
            }
            </p>

            <TasteText
              is_mobile={props.is_mobile}
              text={props.notif_date}
              type={Vars.TASTE_TEXT_TYPE.DETAIL_INFO}
            />
          </div>
        </div>
        <p className='talk-title'>
          <b className='topic'>
            {props.notification.content.topic.title}
          </b>

          in 

          <span className='group'>
            {props.notification.content.group.title}
          </span>
        </p>
        {
          props.notification.content.body?
            <p className='notification-content-body'>{props.body}</p>
          :
            null
        }
        <style>{`
          a {
            text-decoration: none
          }
          p {
            color: #4a4a4a;
          }
          .notification-text {
            margin: 0 0 5px 0;
          }
          .notification-username {
            font-family: Montserrat;
            font-size: 14px;
            font-weight: 500;
            color: #db284e;
          }
          .notification-type {
            font-family: Montserrat;
            font-size: 14px;
            font-weight: 500;
            color: #4a4a4a;
          }
          .notification-user {
            display: flex;
            align-items: center;
          }
          .notification-user {
            margin: 0 0 10px 0;
          }
          .notification-content-body {
            font-family: Montserrat;
            font-size: 14px;
            font-weight: 500;
            margin: 0;
          }
          .notification-card {
            display: flex;
            flex-flow: column;
            justify-content: center;
          }
          .user-image {
            width: 35px;
            height: 35px;
            margin: 0 15px 0 0;
            flex-shrink: 0;
          }
          .user-info {

          }
          .talk-title {
            font-family: Montserrat;
            font-size: 16px;
            font-weight: 300;
            margin: 5px 0 15px;
            padding: 0;
          }
          .talk-title .topic {
            font-weight: 700;
            color: #000;
            margin: 0 5px 0 0;
          }
          .talk-title .group {
            color: #db284e;
            margin: 0 0 0 5px;
          }
          
        `}</style>
      </a>
    </Link>
    
  )
}

export default TasteNotificationCardMobile