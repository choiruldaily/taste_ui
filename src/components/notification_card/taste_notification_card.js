import React from 'react'
import PropTypes from 'prop-types'

import { moment_date } from '../../helpers/utils'

import TasteNotificationCardMobile from './taste_notification_card_mobile'

const TasteNotificationCard = ({ is_mobile, notification={}, image_endpoint }) => {
  const props = { 
                  notification: notification || {},
                  is_mobile   : is_mobile,
                  body        : notification && notification.content.body ? notification.content.body.length>60?notification.content.body.substring(0,60)+'...':notification.content.body : '',
                  username    : notification.author.id ? notification.author.username || '-' : '-',
                  image_url   : notification.author.picture ? (notification.author.picture.small.split('://').length>1 ? '': image_endpoint)+notification.author.picture.small : 'http://pauillac.inria.fr/~harley/pics/square.gif',
                  notif_date  : notification && notification.created_at ? moment_date({ date: notification.created_at }) : '',
                }
  
  if (is_mobile) {
    return (
      <TasteNotificationCardMobile
        { ...props }
      />
    )
  } else {
    return (
      <TasteNotificationCardMobile
        { ...props }
      />
    )
  }
}

TasteNotificationCard.propTypes = {
  is_mobile   : PropTypes.bool.isRequired,
  notification        : PropTypes.object.isRequired,
  // home_endpoint   : PropTypes.string.isRequired
}

export default TasteNotificationCard