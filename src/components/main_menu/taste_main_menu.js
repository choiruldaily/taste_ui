import React from 'react'
import PropTypes from 'prop-types'

import TasteMainMenuMobile from './taste_main_menu_mobile'

const TasteMainMenu = ({ is_mobile, menus, selected, on_click }) => {
  const props = {
    is_mobile : is_mobile || true,
    menus     : menus || [],
    selected,

    on_click
  }
  
  if (is_mobile) {
    return (
      <TasteMainMenuMobile
        { ...props }
      />
    )
  } else {
    return (
      <TasteMainMenuMobile
        { ...props }
      />
    )
  }
}

TasteMainMenu.propTypes = {
  is_mobile   : PropTypes.bool.isRequired,
  menus       : PropTypes.array.isRequired,
  // home_endpoint   : PropTypes.string.isRequired
}

export default TasteMainMenu