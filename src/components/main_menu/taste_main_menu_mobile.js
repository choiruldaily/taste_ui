import React from 'react'
import Link from 'next/link'

import Vars from '../../consts/vars'

import TasteTitle from '../../elements/title/taste_title';

const TasteMainMenuMobile = (props) => {
  return (
    <div className='taste-main-menu'>

      {
        props.menus.map((item,idx)=>
          <Link
            key={idx}
            href={{ pathname: item.pathname, query: item.query }}
            as={item.as_path}
            
          >
            {/* <a onClick={()=>props.on_click(item.value)}> */}
            <a>
              <TasteTitle
                text={item.name}
                type={props.selected === item.value? Vars.TASTE_TITLE_TYPE.NAVIGATION_SELECTED : Vars.TASTE_TITLE_TYPE.NAVIGATION}
                additional_class={props.selected === item.value?'menu-item-selected':'menu-item'}
                is_mobile={true}
              />
            </a>
          </Link>
        )
      }
      
      <style>{`
        .taste-main-menu {
          display: flex;
          flex-flow: row;
          align-items: center;
          justify-content: space-evenly;
          width: 100%;
          padding: 10px;
          border-bottom: 1px solid #efefef;
        }        
      `}</style>
    </div>
  )
}

export default TasteMainMenuMobile