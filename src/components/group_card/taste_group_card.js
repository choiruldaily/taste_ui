import React from 'react'
import PropTypes from 'prop-types'

import TasteGroupCardMobile from './taste_group_card_mobile'

const TasteGroupCard = ({ is_mobile, group, is_hide_join, host, groups_asset_endpoint, style, on_join }) => {
  const props = { 
    is_mobile               : is_mobile || true, 
    group                   : group,
    host                    : host || '',
    groups_asset_endpoint   : groups_asset_endpoint || '',
    is_hide_join            : is_hide_join,
    style                   : style || '',
    button_text             : group.is_join ? 'MEMBER' : '+ JOIN',
    pathname                : '/group',
    query                   : { group: group.title_slug, q: '', order: 'newest', page: 1 },
    as_path                 : `/groups/${group.title_slug}?q=&order=newest&page=1`,

    handle_join
  }
  
  if (is_mobile) {
    return (
      <TasteGroupCardMobile
        { ...props }
      />
    )
  } else {
    return (
      <TasteGroupCardMobile
        { ...props }
      />
    )
  }

  function handle_join() {
    on_join(group)
  }
}

TasteGroupCard.propTypes = {
  is_mobile   : PropTypes.bool.isRequired,
  group        : PropTypes.object.isRequired
}

export default TasteGroupCard

/**
 * group
 * - id
 * - name
 * - slug
 * - info: {}
 * - user: {}
 */