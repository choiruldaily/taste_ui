import React from 'react'
import Link from 'next/link'

import Vars from '../../consts/vars'

import TasteImage from '../../elements/image/taste_image'
import TasteTitle from '../../elements/title/taste_title'
import TasteText from '../../elements/text/taste_text'
import TasteInfoIcon from '../../elements/info_icon/taste_info_icon'
import TasteButton from '../../elements/button/taste_button'

const TasteGroupCardMobile = (props) => {
  return (

    <div className='taste-group-card'>

      {
        props.host?
          <React.Fragment>
            <Link>
              <a href={`${props.host}${props.as_path}`}>
                <TasteImage
                  is_mobile={props.is_mobile}
                  url={`${props.groups_asset_endpoint}/${props.group.group_picture}`}
                  // url='http://pauillac.inria.fr/~harley/pics/square.gif'
                  style={'width:110px;height:110px;margin:10px;'}
                  type= {Vars.TASTE_IMAGE_TYPE.CIRCLE}
                />
              </a>
            </Link>
            <Link>
              <a href={`${props.host}${props.as_path}`}>
                <TasteTitle
                  is_mobile={props.is_mobile}
                  type={Vars.TASTE_TITLE_TYPE.SUB_TITLE}
                  text={props.group.title || 'Taste Title'}
                  style={'text-align:center;margin:10px 0;'}
                  additional_class={'group-title'}
                />
              </a>
            </Link>
          </React.Fragment>
        : <React.Fragment>
            <Link 
              href={{
                pathname  : props.pathname,
                query     : props.query
              }}
              as={props.as_path}
            >
              <a>
                <TasteImage
                  is_mobile={props.is_mobile}
                  url={`${props.groups_asset_endpoint}/${props.group.group_picture}`}
                  // url='http://pauillac.inria.fr/~harley/pics/square.gif'
                  style={'width:110px;height:110px;margin:10px;'}
                  type= {Vars.TASTE_IMAGE_TYPE.CIRCLE}
                />
              </a>
            </Link>

            <Link 
              href={{
                pathname  : props.pathname,
                query     : props.query
              }}
              as={props.as_path}
            >
              <a>
                <TasteTitle
                  is_mobile={props.is_mobile}
                  type={Vars.TASTE_TITLE_TYPE.SUB_TITLE}
                  text={props.group.title || 'Taste Title'}
                  style={'text-align:center;margin:10px 0;'}
                  additional_class={'group-title'}
                />
              </a>
            </Link>
          </React.Fragment>
      }      

      <div className="taste-group-card-info">
        <div className='info-icon'>
          <span className="icon-icon_member">
            <span className="path1"></span><span className="path2"></span>
          </span>
          <TasteText
            text={props.group.total_subscribers||'-'}
            is_mobile={true}
            type={Vars.TASTE_TEXT_TYPE.DETAIL_NAME}
          />
        </div>

        <div className='info-icon'>
          <span className="icon-icon_topic ic-topic">
          </span>
          <TasteText
            text={props.group.total_topics||'-'}
            is_mobile={true}
            type={Vars.TASTE_TEXT_TYPE.DETAIL_NAME}
          />
        </div>

        <div className='info-icon'>
          <span className="icon-icon_post ic-post">
          </span>
          <TasteText
            text={props.group.total_talks||'-'}
            is_mobile={true}
            type={Vars.TASTE_TEXT_TYPE.DETAIL_NAME}
          />
        </div>
      </div>

      <TasteText
        is_mobile={props.is_mobile}
        text={props.group.description || 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.'}
        type={Vars.TASTE_TEXT_TYPE.DETAIL_NAME}
        style={'text-align:center; margin:10px;'}
        additional_class={'desc'}
      />

      {
        props.is_hide_join?
          null
        : props.group.is_join?
          <TasteButton
            is_mobile={props.is_mobile}
            type={Vars.TASTE_BUTTON_TYPE.WHITE_ROUND}
            text={props.button_text}
            class_icon={'icon-icon_check'}
            style={'margin-top:auto; width:inherit; justify-content:center; letter-spacing:5px;'}
            style_icon={'font-size: 12px;margin: 2px 2px 0 0;'}
            on_click={props.handle_join}
            additional_class={'member'}
          />
        :
          <TasteButton
            is_mobile={props.is_mobile}
            type={Vars.TASTE_BUTTON_TYPE.PINK_ROUND}
            text={props.button_text}
            style={'margin-top:auto; width:inherit; justify-content:center; letter-spacing:5px;'}

            on_click={props.handle_join}
          />
      }

      <style>{`
        .taste-text-3 {
          margin: 0 10px 0 5px;
        }
        .ic-post {
          font-size: 14px;
        }
        .ic-topic {
          font-size: 12px;
        }
        .taste-group-card {
          word-break: break-word;
          display: flex;
          flex-flow: column;
          width: 150px;
          height: 100%;
          align-items: center;
          box-shadow: -3px 4px 10px 0 rgba(0, 0, 0, 0.08);
          ${props.style}
        }
        .taste-group-card-info {
          display: flex;
          flex-flow: wrap;
          justify-content: center;
          padding: 0 10px;
        }
        .info-icon {
          display: flex;
          align-items: center;
          flex-flow: row;
          margin: 2px 0;
        }
        
      `}</style>
    </div>
  )
}

export default TasteGroupCardMobile