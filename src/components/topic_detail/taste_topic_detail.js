import React from 'react'
import PropTypes from 'prop-types'

import { moment_date } from '../../helpers/utils'

import TasteTopicDetailMobile from './taste_topic_detail_mobile'

const TasteTopicDetail = ({ is_mobile, topic, on_edit, topics_asset_endpoint, profile_image_endpoint, handle_modal }) => {
  const props = { 
    is_mobile               : is_mobile || true, 
    topic                   : topic || {}, 
    images                  : topic.images || [],
    video                   : topic.videos && topic.videos.length>0 ? topic.videos[0] : [],
    topic_date              : topic && topic.created_at ? moment_date({ date: topic.created_at }) : '',
    topics_asset_endpoint   : topics_asset_endpoint || '',
    profile_image_endpoint  : profile_image_endpoint || '',
    url: {
      pathname  : '/topic',
      query     : { topic: topic.title_slug },
      as        : `/topics/${topic.title_slug}`
    },

    on_edit,
    handle_modal
  }
  
  if (is_mobile) {
    return (
      <TasteTopicDetailMobile
        { ...props }
      />
    )
  } else {
    return (
      <TasteTopicDetailMobile
        { ...props }
      />
    )
  }
}

TasteTopicDetail.propTypes = {
  is_mobile   : PropTypes.bool.isRequired,
  topic       : PropTypes.object.isRequired
}

export default TasteTopicDetail

/**
 * topic
 * - id
 * - name
 * - slug
 * - info: {}
 * - user: {}
 */