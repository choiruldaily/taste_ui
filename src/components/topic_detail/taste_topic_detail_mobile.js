import React from 'react'
import Link from 'next/link'

import Vars from '../../consts/vars'

import TasteImage from '../../elements/image/taste_image'
import TasteVideo from '../../elements/video/taste_video'
import TasteTitle from '../../elements/title/taste_title'
import TasteText from '../../elements/text/taste_text'
import TasteInfoIcon from '../../elements/info_icon/taste_info_icon'
import TasteTag from '../../elements/tag/taste_tag'
import TasteUserCard from '../../components/user_card/taste_user_card'

const TasteTopicDetailMobile = (props) => {
  return (
    <div className='taste-topic-detail'>
      <div className='topic-user'>
        <TasteText
          is_mobile={props.is_mobile}
          text={props.topic_date}
          type={Vars.TASTE_TEXT_TYPE.TIME}
          style={'text-align:right;margin-bottom:-20px;'}
        />
        <TasteUserCard
          is_mobile={props.is_mobile}
          user={props.topic.user}
          image_endpoint={props.profile_image_endpoint}
        />
      </div>

      <TasteTitle
        is_mobile={props.is_mobile}
        text={props.topic.title}
        type={Vars.TASTE_TITLE_TYPE.DEFAULT}
        additional_class={'topic-title-detail'}
      />
      
      <div className="taste-topic-detail-info">

        <div className='info-icon-topic'>
          <span className="icon-icon_member ictd-member">
            <span className="path1"></span><span className="path2"></span>
          </span>
          <TasteText
            text={props.topic.total_users||'-'}
            is_mobile={true}
            type={Vars.TASTE_TEXT_TYPE.INFOBAR_TOPIC}
          />
        </div>

        <div className='info-icon-topic'>
          <span className="icon-icon_post ictd-post">
          </span>
          <TasteText
            text={props.topic.total_talks||'-'}
            is_mobile={true}
            type={Vars.TASTE_TEXT_TYPE.INFOBAR_TOPIC}
          />
        </div>
        {
          props.topic.is_owner?
            <span className='icon-icon_more ictd-more' onClick={()=>{ props.on_edit(props.topic) }}/>
          :
            null
        }
      </div>

      <TasteText
        is_mobile={props.is_mobile}
        text={props.topic.description || ''}
      />
      <div className='tags'>
        <TasteText
          is_mobile={props.is_mobile}
          text={'Tags'}
        />

        {
          props.topic.tags && props.topic.tags.map((item,idx)=>
            <TasteTag
              key={idx}
              is_mobile={props.is_mobile}
              text={item}
              
            />
          )
        }
      </div>
      <div className='topic-images'>
        {
          props.images.map((item,idx)=>
            <TasteImage
              key={idx}
              is_mobile={props.is_mobile}
              type={Vars.TASTE_IMAGE_TYPE.CLICKABLE}
              url={`${props.topics_asset_endpoint}/${item}`}
              style={'border-radius: 4px;width:85px; height:85px; margin-right:10px; justify-content: center; align-items: center; overflow: hidden;'}
              additional_class={'topic-detail-media'}
              
              on_click={props.handle_modal}
            />
          )
        }
      </div>

      {
        props.video.thumbnail?
          <div className='topic-videos'>
            <TasteImage
              is_mobile={props.is_mobile}
              type={Vars.TASTE_IMAGE_TYPE.CLICK_VIDEO}
              url={`${props.topics_asset_endpoint}/${props.video.thumbnail}`}
              url_video={`${props.topics_asset_endpoint}/${props.video.video}`}
              style={'border-radius: 4px;width:85px; height:85px; margin-right:10px; justify-content: center; align-items: center; overflow: hidden;'}
              additional_class={'video-topic'}

              on_click={props.handle_modal}
            />
          </div>
        :
          null
      }

      

      <style>{`
        .ic-play {
          position: absolute;
          z-index: 1;
          font-size: 32px;
        }
        .sample {
          flex-shrink: 0;
          display: flex;
          background-color: #fff;
          position: relative;
          justify-content: center;
          align-items: center;
        }
        .taste-topic-detail {
          display: flex;
          flex-flow: column;
          width: 320px;
          border-bottom: 1px solid #efefef;
          padding: 20px;
          background-color: #ffffff;
          word-break: break-word;
        }
        .tags {
          width: 100%;
          display: flex;
          align-items: center;
          flex-flow: wrap;
        }
        .taste-topic-detail-info {
          display: flex;
          flex-flow: wrap;
          width: 100%;
          margin: 15px 0;
          align-items: center;
        }
        .ictd-more {
          margin-left: auto;
          font-size: 6px;
        }
        .ictd-member {
          font-size: 18px;
          margin: 0 10px 0 0;
        }
        .ictd-post {
          font-size: 16px;
          margin: 0 10px 0 20px;
        }
        .info-icon-topic {
          display: flex;
          align-items: center;
          flex-flow: row;
          margin: 5px 0;
        }
        .topic-image-item {
          margin-right: 10px;
        }
        .topic-images {
          width: 100%;
          display: flex;
          flex-flow: wrap;
          margin: 15px 0 10px 0;
        }
        .topic-images:empty {
          display: none;
        }
        .topic-videos {
          width: 100%;
          display: flex;
          flex-flow: wrap;
          margin: 15px 0 10px 0;
        }
        .topic-videos:empty {
          display: none;
        }
        .topic-user {
          display: flex;
          flex-flow: column;
          width: 100%;
          margin: 0 0 20px 0;
        }
      `}</style>
    </div>
  )
}

export default TasteTopicDetailMobile