import React from 'react'
import PropTypes from 'prop-types'

import Vars from '../../consts/vars'

import TastePaginationMobile from './taste_pagination_mobile'

const TastePagination = ({ is_mobile, page, per_page, total_page, scroll_id, button_top_id, update_page, page_start}) => {
  const props = {
    is_mobile         : is_mobile || true,
    page              : page,
    scroll_id         : scroll_id || 'top-page',
    button_top_id     : button_top_id || scroll_id,
    total_page        : total_page || 0,
    per_page          : per_page || Vars.PER_PAGE_10,
    page_start        : is_mobile?
                          // Pagination Mobile
                          (page>4&&total_page>Vars.PAGE_PER_SECTION_MOBILE&&page>total_page-3)?
                            total_page-4
                          :
                            (page>4&&total_page>Vars.PAGE_PER_SECTION_MOBILE)?
                              page-2
                            :
                              1
                        :
                          // Pagination Desktop
                          (page>5&&total_page>Vars.PAGE_PER_SECTION&&page>total_page-4)?
                            total_page-6
                          :
                            (page>5&&total_page>Vars.PAGE_PER_SECTION)?
                              page-3
                            :
                              1
                        ,
    page_list         : is_mobile?
                          // Mobile
                          total_page<=Vars.PAGE_PER_SECTION_MOBILE || total_page>Vars.PAGE_PER_SECTION_MOBILE && page<5?
                            [0,1,2,3,4,5]
                          : 
                            [0,1,2,3,4]
                          // Desktop
                        : 
                          total_page<=Vars.PAGE_PER_SECTION || total_page>Vars.PAGE_PER_SECTION && page<6?
                            [0,1,2,3,4,5,6,7]
                          : 
                            [0,1,2,3,4,5,6],
    update_page,
    scroll_to
  }

  if (is_mobile) {
    return (
      <TastePaginationMobile
        { ...props }
      />
    )
  } else {
    return (
      <TastePaginationMobile
        { ...props }
      />
    )
  }

  function scroll_to (element, to, duration) {
    /*
    const item  = document.getElementById(scroll_id || 'root-container'),
          diff  = (item.offsetTop-window.scrollY)/8
          
    if (Math.abs(diff)>1) {
        window.scrollTo(0, (window.scrollY+diff))
        clearTimeout(window._TO)
        window._TO=setTimeout(scroll_to_item, 30, item)
    } else {
        window.scrollTo(0, item.offsetTop)
    }
    */
    if (typeof window !== 'undefined') {
      const elm = document.getElementById(element)
      if (elm) elm.scrollIntoView({behavior: "smooth"})
    }
  }
}

TastePagination.propTypes = {
  is_mobile   : PropTypes.bool.isRequired,
}

export default TastePagination