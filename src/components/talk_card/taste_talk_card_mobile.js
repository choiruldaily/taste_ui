import React from 'react'
import Link from 'next/link'
import Vars from '../../consts/vars'

import TasteImage from '../../elements/image/taste_image'
import TasteVideo from '../../elements/video/taste_video'
import TasteTitle from '../../elements/title/taste_title'
import TasteText from '../../elements/text/taste_text'
import TasteInfoIcon from '../../elements/info_icon/taste_info_icon'
import TasteTag from '../../elements/tag/taste_tag'
import TasteUserCard from '../user_card/taste_user_card'

const TasteTalkCardMobile = (props) => {

  return (
    <div className='taste-talk-card'>
      <div className='talk-user'>
        <TasteText
          is_mobile={props.is_mobile}
          text={props.talk_date}
          type={Vars.TASTE_TEXT_TYPE.TIME}
          style={'text-align:right;margin-bottom:-20px;'}
        />
        {
          props.is_user_card?
            <TasteUserCard
              is_mobile={props.is_mobile}
              user={props.talk.user}
              image_endpoint={props.profile_image_endpoint}
            />
          :
            null
        }
      </div>
      {
        props.is_title?
          <p className='talk-title'>

            {
              props.host?
                <React.Fragment>
                  <Link href={props.host+props.topic_url.as_path}>
                    <a className='topic'>
                      {props.talk.topic.title}
                    </a>
                  </Link>

                  in 
                  <Link href={props.host+props.group_url.as_path}>
                    <a className='group'>
                      {props.talk.group.title}
                    </a>
                  </Link>
                </React.Fragment>
              : <React.Fragment>
                  {
                    props.topic_url.query.topic?
                      <Link 
                        href={{
                          pathname  : props.topic_url.pathname,
                          query     : props.topic_url.query
                        }}
                        as={props.topic_url.as_path}
                      >
                        <a className='topic'>
                          {props.talk.topic.title}
                        </a>
                      </Link>
                    : <span className='topic'>
                        {props.talk.topic.title}
                      </span>
                  }
                  
                  
                  in 

                  {
                    props.group_url.query.group?
                      <Link 
                        href={{
                          pathname  : props.group_url.pathname,
                          query     : props.group_url.query
                        }}
                        as={props.group_url.as_path}
                      >
                        <a className='group'>
                          {props.talk.group.title}
                        </a>
                      </Link>
                    : <span className='group'>
                        {props.talk.group.title}
                      </span>
                  }
                  
                  
                </React.Fragment>
            }
            
          </p>
        :
          null
      }
      <p className='talk-body'>
        {
          props.reply_to?
            <Link href={props.user_url}>
              <a className='talk-mention'>
                {'@'+props.reply_to+' '}
              </a>
            </Link>
          :
            null
        }
        {props.talk.body || ''} 
      </p>

      <div className='talk-images'>
        {
          props.images.length>3?
            props.images.map((item,idx)=>
              idx<2?

                <TasteImage
                  key={idx}
                  is_mobile={props.is_mobile}
                  type={Vars.TASTE_IMAGE_TYPE.CLICKABLE}
                  url={`${props.talks_asset_endpoint}/${item}`}
                  style={'border-radius: 4px;width:85px; height:85px; margin-right:10px; justify-content: center; align-items: center; overflow: hidden;'}

                  additional_class={'list-talk'}
                  on_click={props.handle_modal}
                />
              : idx<3?
                <TasteImage
                  key={idx}
                  is_mobile={props.is_mobile}
                  text={'2'}
                  type={Vars.TASTE_IMAGE_TYPE.TEXT}
                  url={`${props.talks_asset_endpoint}/${item}`}
                  style={'border-radius: 4px;width:85px; height:85px; margin-top: 10px;'}
                />
              : null
            )
          :
            props.images.map((item,idx)=>
              <TasteImage
                key={idx}
                is_mobile={props.is_mobile}
                type={Vars.TASTE_IMAGE_TYPE.CLICKABLE}
                url={`${props.talks_asset_endpoint}/${item}`}
                style={'border-radius: 4px;width:85px; height:85px; margin-right:10px; justify-content: center; align-items: center; overflow: hidden;'}

                additional_class={'list-talk'}

                on_click={props.handle_modal}
              />
            )
        }
      </div>
      {
        props.video.thumbnail?
          <div className='talk-videos'>
            <TasteImage
              is_mobile={props.is_mobile}
              type={Vars.TASTE_IMAGE_TYPE.CLICK_VIDEO}
              url={`${props.talks_asset_endpoint}/${props.video.thumbnail}`}
              url_video={`${props.talks_asset_endpoint}/${props.video.video}`}
              style={'border-radius: 4px;width:85px; height:85px; margin-right:10px; justify-content: center; align-items: center; overflow: hidden;'}
              additional_class={'video-talk'}

              on_click={props.handle_modal}
            />
          </div>
        :
          null
      }
      <div className='talk-bottom'>
        <div className='ic-action'>
          <div className='ic-like-contain'>
            <i className={`${props.talk.is_like?'icon-icon_liked':'icon-icon_like'}`+' ic-like'} onClick={()=>{ props.on_like(props.talk) }}/>
            <TasteText
              is_mobile={props.is_mobile}
              text={props.talk.total_likes || '0'}
              type={Vars.TASTE_TEXT_TYPE.INFOBAR_POST}
            />
          </div>
          {
            props.is_reply?
              <div className='ic-like-contain'>
                <i className='icon-icon_reply ic-reply' onClick={()=>{ props.on_reply(props.talk) }}/>
                <TasteText
                  is_mobile={props.is_mobile}
                  text={props.talk.total_replies || '0'}
                  type={Vars.TASTE_TEXT_TYPE.INFOBAR_POST}
                />
              </div>
              
            :
              null
          }
          
        </div>
        {
          props.talk.is_owner && props.is_reply ? 
            <i className='icon-icon_more ic-more' onClick={()=>{props.on_edit(props.talk)}}/>
          :            
            null
        }
      </div>
      <style>{`
        .talk-body {
          font-family: Montserrat;
          font-size: 14px;
          font-weight: 500;
          color: #4a4a4a;
          margin: 0;
        }
        .talk-mention {
          color: #db284e;
        }
        .ic-action {
          display: flex;
        }
        .ic-like-contain {
          min-width: 75px;
          display: flex;
          align-items: center;
        }
        .talk-title {
          font-family: Montserrat;
          font-size: 16px;
          font-weight: 300;
          margin: 5px 0 15px;
          padding: 0;
        }
        .talk-title .topic {
          font-weight: 700;
          color: #000;
          margin: 0 5px 0 0;
        }
        .talk-title .group {
          color: #db284e;
          margin: 0 0 0 5px;
        }
        .talk-bottom {
          margin: 20px 0 0 0;
          display: flex;
          align-items: center;
          justify-content: space-between;
          flex-flow: row;
        }
        .ic-like {
          font-size: 20px;
          margin: 0 5px 0 0;
        }
        .ic-reply {
          font-size: 18px;
          margin: 0 5px 0 0;
        }
        .ic-more {
          font-size: 6px;
        }
        .talk-images {
          width: 100%;
          display: flex;
          flex-flow: wrap;
          margin: 15px 0 10px 0;
        }
        .talk-images:empty {
          display: none;
        }
        .talk-videos {
          width: 100%;
          display: flex;
          flex-flow: row;
          margin: 15px 0 10px 0;
        }
        .talk-videos:empty {
          display: none;
        }
        .taste-talk-card {
          display: flex;
          flex-flow: column;
          padding: 20px;
          background-color: #ffffff;
          word-break: break-word
        }
        .taste-talk-card-info {
          display: flex;
          flex-flow: wrap;
          width: 100%;
          margin: 15px 0;
        }
        .talk-user {
          display: flex;
          flex-flow: column;
          width: 100%;
          margin: 0 0 20px 0;
        }
      `}</style>
    </div>
  )
}

export default TasteTalkCardMobile