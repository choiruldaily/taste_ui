import React from 'react'
import Link from 'next/link'
import Vars from '../../consts/vars'

import TasteImage from '../../elements/image/taste_image'
import TasteTitle from '../../elements/title/taste_title'
import TasteText from '../../elements/text/taste_text'
import TasteInfoIcon from '../../elements/info_icon/taste_info_icon'
import TasteTag from '../../elements/tag/taste_tag'
import TasteUserCard from '../user_card/taste_user_card'

const TasteTalkCardDesktop = (props) => {
  return (
    <div className='taste-talk-card-desktop'>
      {
        props.is_user_card?
          <div className='talk-user'>
            <TasteUserCard
              is_mobile={props.is_mobile}
              user={props.talk.user}
              image_endpoint={props.image_profile_endpoint}
            />
          </div>
        :
          null
      }
     
      <div className='taste-talk-contain'>
        <TasteText
          is_mobile={props.is_mobile}
          text={props.talk_date}
          type={Vars.TASTE_TEXT_TYPE.TIME}
          style={'text-align:right;margin-bottom:-20px;'}
        />
        {
          props.is_title?
            <p className='talk-title'>

              {
                props.host?
                  <React.Fragment>
                    <Link href={props.host+props.topic_url.as_path}>
                      <a className='topic'>
                        {props.talk.topic.title}
                      </a>
                    </Link>

                    in 
                    <Link href={props.host+props.group_url.as_path}>
                      <a className='group'>
                        {props.talk.group.title}
                      </a>
                    </Link>
                  </React.Fragment>
                : <React.Fragment>
                    {
                      props.topic_url.query.topic?
                        <Link 
                          href={{
                            pathname  : props.topic_url.pathname,
                            query     : props.topic_url.query
                          }}
                          as={props.topic_url.as_path}
                        >
                          <a className='topic'>
                            {props.talk.topic.title}
                          </a>
                        </Link>
                      : <span className='topic'>
                          {props.talk.topic.title}
                        </span>
                    }
                    
                    
                    in 

                    {
                      props.group_url.query.group?
                        <Link 
                          href={{
                            pathname  : props.group_url.pathname,
                            query     : props.group_url.query
                          }}
                          as={props.group_url.as_path}
                        >
                          <a className='group'>
                            {props.talk.group.title}
                          </a>
                        </Link>
                      : <span className='group'>
                          {props.talk.group.title}
                        </span>
                    }
                    
                    
                  </React.Fragment>
              }
              
            </p>
          :
            null
        }
        <p className='talk-body'>
          {
            props.reply_to?
              <Link href={props.user_url}>
                <a className='talk-mention'>
                  {'@'+props.reply_to+' '}
                </a>
              </Link>
            :
              null
          }
          {props.talk.body || ''} 
        </p>

        <div className='talk-image'>
          {
            props.talk.media?
              props.talk.media.length>3?
                props.talk.media.map((item,idx)=>
                  idx<2?
                    <TasteImage
                      key={idx}
                      is_mobile={props.is_mobile}
                      url={`${props.image_talk_endpoint}/${item.url}`}
                      style={'width:150px; height:150px; margin-right:10px;'}
                    />
                  : idx<3?
                    <TasteImage
                      key={idx}
                      is_mobile={props.is_mobile}
                      text={'2'}
                      type={Vars.TASTE_IMAGE_TYPE.TEXT}
                      url={`${props.image_talk_endpoint}/${item.url}`}
                      style={'width:150px; height:150px; margin-right:10px;'}
                    />
                  : null
                )
              :
                props.talk.media.map((item,idx)=>
                  <TasteImage
                    key={idx}
                    is_mobile={props.is_mobile}
                    url={`${props.image_talk_endpoint}/${item.url}`}
                    style={'width:150px; height:150px; margin-right:10px;'}
                  />
                )
            : null
          }
        </div>
        <div className='talk-bottom'>
          <div className='ic-like-contain'>
            <i className={`${props.talk.is_like?'icon-icon_liked':'icon-icon_like'}`+' ic-like'} onClick={()=>{ props.on_like(props.talk) }}/>
            <TasteText
              is_mobile={props.is_mobile}
              text={props.talk.total_likes!==0?props.talk.total_likes>1?props.talk.total_likes+' Likes':props.talk.total_likes+' Like':'Like' || ''}
              type={Vars.TASTE_TEXT_TYPE.INFOBAR_POST}
            />
          </div>
          {
            props.is_reply?
              <div className='ic-like-contain'>
                <i className='icon-icon_reply ic-reply' onClick={()=>{ props.on_reply(props.talk) }}/>
                <TasteText
                  is_mobile={props.is_mobile}
                  text={props.talk.total_replies || '0'}
                  type={Vars.TASTE_TEXT_TYPE.INFOBAR_POST}
                />
              </div>
              
            :
              null
          }
          <i className='icon-icon_more ic-more'/>
        </div>
      </div>

      <style>{`
        .talk-body {
          font-family: Montserrat;
          font-size: 14px;
          font-weight: 500;
          color: #4a4a4a;
          margin: 0;
        }
        .talk-mention {
          color: #db284e;
        }
        .taste-talk-contain {
          word-break: break-word;
          width: 100%;
        }
        .ic-action {
          display: flex;
        }
        .ic-like-contain {
          min-width: 75px;
          display: flex;
          align-items: center;
          margin: 0 20px 0 0;
        }
        .talk-title {
          font-family: Montserrat;
          font-size: 24px;
          font-weight: 300;
          margin: 0 0 15px;
          padding: 0;
          max-width: 90%;
        }
        .talk-title .topic {
          font-weight: 700;
          color: #000;
          margin: 0 5px 0 0;
        }
        .talk-title .group {
          color: #db284e;
          margin: 0 0 0 5px;
        }
        .talk-bottom {
          border-top: 1px solid #efefef;
          padding: 10px 0 0 0;
          margin: 20px 0 0 0;
          display: flex;
          align-items: center;
          justify-content: flex-end;
          flex-flow: row;
        }
        .ic-like {
          font-size: 20px;
          margin: 0 5px 0 0;
        }
        .ic-reply {
          font-size: 18px;
          margin: 0 5px 0 0;
        }
        .ic-more {
          font-size: 6px;
        }
        .talk-image {
          width: 100%;
          display: flex;
          flex-flow: row;
          margin: 20px 0 10px 0;
        }
        .talk-image:empty {
          display: none;
        }
        .taste-talk-card-desktop {
          display: flex;
          flex-flow: row;
          padding: 20px;
          background-color: #ffffff;
          border-radius: 8px;
        }
        .taste-talk-card-desktop-info {
          display: flex;
          flex-flow: wrap;
          width: 100%;
          margin: 15px 0;
        }
        .talk-user {
          display: flex;
          flex-flow: column;
          width: 143px;
          flex-shrink: 0;
        }
      `}</style>
    </div>
  )
}

export default TasteTalkCardDesktop