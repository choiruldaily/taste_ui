import React from 'react'
import PropTypes from 'prop-types'

import { moment_date } from '../../helpers/utils'

import TasteTalkCardMobile from './taste_talk_card_mobile'
import TasteTalkCardDesktop from './taste_talk_card_desktop'

const TasteTalkCard = ({ 
  is_mobile, 
  talk, 
  host, 
  host_review, 
  talks_asset_endpoint, 
  profile_image_endpoint, 
  is_title, 
  is_reply, 
  is_user_card, 
  on_reply, 
  on_edit,
  on_like,
  handle_modal }) => {

  const props = { 
    is_mobile               : is_mobile, 
    is_title                : is_title || false, 
    is_reply                : is_reply || false, 
    is_user_card            : is_user_card || false, 
    talk                    : talk || {},
    images                  : talk.images || [],
    video                   : talk.videos && talk.videos.length>0 ? talk.videos[0] : [],
    talk_date               : talk && talk.created_at ? moment_date({ date: talk.created_at }) : '',
    reply_to                : talk.reply_to && talk.reply_to.username? talk.reply_to.username : '',
    host                    : host || '',
    talks_asset_endpoint    : talks_asset_endpoint || '',
    profile_image_endpoint  : profile_image_endpoint || '',
    group_url: {
      pathname  : '/group',
      query     : { group: talk.group? talk.group.title_slug : '', q: '', order: 'newest', page: 1 },
      as_path   : `/groups/${talk.group? talk.group.title_slug : ''}?q=&order=newest&page=1`
    },
    topic_url: {
      pathname  : '/topic',
      query     : { topic: talk.topic ? talk.topic.title_slug : '', order: 'newest', page: 1 },
      as_path   : `/topics/${talk.topic? talk.topic.title_slug : ''}?order=newest&page=1`
    },
    user_url    : `${host_review}/user/${talk.reply_to && talk.reply_to.username? talk.reply_to.username : ''}?tab=reviews`,

    on_reply,
    on_like,
    on_edit,
    handle_modal
  }
  
  if (is_mobile) {
    return (
      <TasteTalkCardMobile
        { ...props }
      />
    )
  } else {
    return (
      <TasteTalkCardDesktop
        { ...props }
      />
    )
  }
}

TasteTalkCard.propTypes = {
  is_mobile   : PropTypes.bool.isRequired,
  talk       : PropTypes.object.isRequired
}

export default TasteTalkCard

/**
 * talk
 * - id
 * - name
 * - slug
 * - info: {}
 * - user: {}
 */