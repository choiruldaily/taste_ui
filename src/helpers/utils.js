import moment from 'moment'

export const moment_date = ({date}) => {
  const local_date = utc_to_local({date}),
        res = moment(local_date).add(7,'days').isBefore(moment())?moment(local_date).format('DD MMM YYYY'):moment(local_date).fromNow()
  return res
}

export const utc_to_local = (params) => {
  const utc = moment.utc(params.date).toDate(),
        res = moment(utc).local().format('YYYY-MM-DD HH:mm:ss')
  return res
}