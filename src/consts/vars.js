export default {
  IMAGE_ENDPOINT    : '//image.femaledaily.com/dyn/600/images/prod-pics/',
  DEFAULT_IMAGE     : '//image.femaledaily.com/dyn/70/images/no_image.jpg',


  // Main Menu
  MAIN_MENU     : [
                    {
                      name    : 'HOME',
                      value   : 'recent',
                      pathname: '/',
                      query   : {},
                      as_path : '/',
                      visible : true
                    },
                    {
                      name    : 'GROUP',
                      value   : 'group',
                      pathname: '/groups',
                      query   : { order: 'newest' },
                      as_path : '/groups?order=newest',
                      visible : true
                    },
                    {
                      name    : 'RECENT',
                      value   : 'recent',
                      pathname: '/recent',
                      query   : {},
                      as_path : '/recent',
                      visible : true
                    }
                  ],

  // Goups Order
  GROUPS_ORDER  : [
                    {
                      name    : 'Newest',
                      value   : 'newest',
                      visible : true
                    },
                    {
                      name    : 'Best Match',
                      value   : 'best_match',
                      visible : false
                    },
                    {
                      name    : 'Most Topics',
                      value   : 'most_topics',
                      visible : true
                    }
                  ],

  //
  TASTE_TEXT_TYPE   : {
                      DEFAULT         : 0,
                      DEFAULT_SPACING : 1,
                      DEFAULT_LIGHT   : 2,
                      DETAIL_NAME     : 3,
                      DETAIL_INFO     : 4,
                      TIME            : 5,
                      MENTION         : 6,
                      INFOBAR_TOPIC   : 7,
                      INFOBAR_POST    : 8,
                      SORT            : 9,
                      PLACEHOLDER     : 10,
                    },
  TASTE_TITLE_TYPE : {
                      DEFAULT             : 0,
                      MAIN_TITLE          : 1,
                      SUB_TITLE           : 2,
                      CARD_TITLE          : 3,
                      NAVIGATION          : 4,
                      NAVIGATION_SELECTED : 5,
                    },
  TASTE_BUTTON_TYPE : {
                      DEFAULT         : 0,
                      WHITE           : 1,
                      WHITE_DEACTIVE  : 2,
                      RED             : 3,
                      RED_DEACTIVE    : 4,
                    },
  TASTE_IMAGE_TYPE : {
                      DEFAULT         : 0,
                      CIRCLE          : 1,
                      TEXT            : 2,
                      CLICKABLE       : 3,
                      CLICK_VIDEO     : 4,
                    },
  TASTE_VIDEO_TYPE : {
                      DEFAULT         : 0,
                      CLICKABLE       : 1,
                    },
  TASTE_BUTTON_TYPE : {
                      DEFAULT     : 0,
                      WHITE       : 1,
                      WHITE_ROUND : 2,
                      PINK        : 3,
                      PINK_ROUND  : 4,
                    },
  MODAL_TYPE        : {
                        IMAGE           : 0,
                        VIDEO           : 1,
                      },
  NOTIFICATIONS_TYPE: {
                        NEW_TALK  : 'new_talk',
                        REPLY     : 'reply',
                        LIKE      : 'like'
                      },
  PAGE_PER_SECTION          : 8,
  PAGE_PER_SECTION_MOBILE   : 6,
  PER_PAGE_3                : 3,
  PER_PAGE_5                : 5,
  PER_PAGE_10               : 10,
  PER_PAGE_15               : 15,
  PER_PAGE_20               : 20,
}