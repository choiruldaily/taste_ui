export default {

  GROUP : {
            admin_id: 28568,
            created_at: "2019-01-14T09:04:58.711Z",
            deleted_at: null,
            description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed molestie nunc quis justo venenatis, a scelerisque velit ultrices. Sed vitae tincidunt eros. Praesent id ullamcorper ex. Phasellus hendrerit id metus at rhoncus. Maecenas non auctor nisi. Ut vestibulum, ante in porttitor gravida, turpis magna",
            group_picture: "300.png",
            id: 2,
            is_join: false,
            tags: {
              0: "tag",
              1: "tag name long super long",
              length: 2,
            },
            title: "Skin Care 101 Female Daily Network",
            title_slug: "skin-care-101",
            total_subscribers: 91,
            total_talks: 100,
            total_topics: 10,
            updated_at: "2019-01-14T09:04:58.711Z"
          },
  GROUP_DETAIL :  {
                    admin:  {
                              age_range: "25 - 29",
                              email: "melinda.young22@gmail.com",
                              fullname: "",
                              id: 28568,
                              level: "Beauty Newbie",
                              user_pics: null,
                              username: "melinda22"
                            },
                    created_at: "2019-01-14T09:04:58.711Z",
                    deleted_at: null,
                    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed molestie nunc quis justo venenatis, a scelerisque velit ultrices. Sed vitae tincidunt eros. Praesent id ullamcorper ex. Phasellus hendrerit id metus at rhoncus. Maecenas non auctor nisi. Ut vestibulum, ante in porttitor gravida, turpis magna",
                    group_picture: "300.png",
                    id: 2,
                    is_join: false,
                    tags: [
                            "tag",
                            "tag name long",
                            "tag name long long",
                            "tag name long super longtagname longsuperlongtag name long super long",
                          ],
                    title: "Skin Care 101",
                    title_slug: "skin-care-101",
                    total_subscribers: 91,
                    total_talks: 100,
                    total_topics: 35,
                    updated_at: "2019-01-14T09:04:58.711Z"
                  },
  USER  : {
            age_range: "19 - 24",
            email: "haaaanihams@gmail.com",
            fullname: "Hanifa",
            id: 51611,
            level: "Beauty Newbie",
            user_pics: null,
            username: "hanihams",
          },
  TOPIC : {
            closed_at: null,
            created_at: "2019-01-14T09:28:36.764Z",
            deleted_at: null,
            description: "udin ligula. Suspendisse potenti. Nulla scelerisque volutpat blandit. Etiam tempor rhoncus nibh, ut porta massa hendrerit ac. Cras libero arcu, facili",
            group_id: 2,
            id: 11,
            is_join_group: false,
            is_owner: false,
            media: [
                    {id: 70, url: "300.png"},
                    {id: 71, url: "300.png"},
                    {id: 72, url: "300.png"},
                    {id: 73, url: "300.png"},
                    // {id: 74, url: "300.png"},
                    // {id: 75, url: "300.png"}
                  ],
            tags: [
                    "tag name",
                    "tag name long",
                    "tag name long super long",
                  ],
            title: "Donec Dictum Magna Turpis, In Pretium Justo Dictum",
            title_slug: "donec-dictum-magna-turpis-in-pretium-justo-dictum-4",
            total_talks: '800M',
            total_users: '104',
            updated_at: "2019-01-14T09:28:36.764Z",
            user : {
              age_range: "19 - 24",
              email: "haaaanihams@gmail.com",
              fullname: "Hanifa",
              id: 51611,
              level: "Beauty Newbie",
              user_pics: null,
              username: "hanihams",
            }
          },
  TALK : {
            body: "Ut nec sodales ligula. Donec molestie tempus consequat. Nullam quis sollicitudin ligula. Suspendisse potenti. Nulla scelerisque volutpat blandit. Etiam tempor rhoncus nibh, ut porta massa hendrerit ac. Cras libero arcu, facilisis vitae ultrices vitae, dignissim quis justo. Nunc varius fringilla urna, nec ornare massa mattis et. Mauris blandit ornare sagittis. Nulla a enim scelerisque, vulputate massa at, tincidunt purus. Phasellus mattis tortor sed nisi aliquet condimentum. Sed sit amet luctus",
            created_at: "2019-01-14T09:46:08.884Z",
            deleted_at: null,
            group:  {
                      id: 8,
                      title: "Absolute Grooming",
                      title_slug: "absolute-grooming"
                    },
            group_id: 2,
            hidden_at: null,
            id: 170,
            is_join_group: false,
            is_owner: false,
            media: [
              {id: 70, url: "300.png"},
              {id: 71, url: "300.png"},
              {id: 72, url: "300.png"},
              {id: 73, url: "300.png"},
              // {id: 74, url: "300.png"},
              // {id: 75, url: "300.png"}
            ],
            parent_post_id: null,
            reply_to_post_id: null,
            topic:{
                    id: 80,
                    title: "Ut Nec Sodales Ligula. Donec Molestie Tempus Conse",
                    title_slug: "ut-nec-sodales-ligula-donec-mo"
                  },
            topic_id: 17,
            total_likes: 0,
            total_replies: 0,
            updated_at: "2019-01-14T09:46:08.884Z",
            user: {
              age_range: "25 - 29",
              email: "nessie_deviana@hotmail.com",
              fullname: "Nessie Deviana",
              id: 56459,
              level: "Beauty Newbie",
              user_pics: [
                            {
                              usrpic_id: 12789,
                              usrpic_userpics_large: "e5ef9d169ec15e2ebdb59f45615fa03b.jpg",
                              usrpic_usrapo_id: 56459
                            }
                          ],
              username: "nessieds"
            }
          },

    NOTIFICATION_CARD : {
                          "content": {
                              "body": "Test 1 Test 1 Test 1 Test 1 Test 1 Test 1 Test 1 Test 1 Test 1 Test 1 Test 1 Test 1 Test 1 Test 1 Test 1 Test 1 Test 1 Test 1 Test 1 Test 1 Test 1 Test 1 Test 1 Test 1 Test 1 Test 1 Test 1 Test 1 Test 1 Test 1 Test 1 Test 1",
                              "group": {
                                  "id": 1,
                                  "title": "Acne Warrior",
                                  "title_slug": "acne-warrior"
                              },
                              "topic": {
                                  "id": 303,
                                  "title": "Test Postmaaaaaaaan",
                                  "title_slug": "test-postmaaaaaaaan-46"
                              }
                          },
                          "type": "new_talk",
                          "owner_id": 359998,
                          "is_read": false,
                          "id": 43,
                          "author": {
                              "id": 297163,
                              "username": "testqa",
                              "picture": {
                                  "small": "http://image.femaledaily.com/dyn/320/images/user-pics/fb8d1be39c81a55436c2b39e1f9eeb63.jpg",
                                  "medium": "http://image.femaledaily.com/dyn/480/images/user-pics/fb8d1be39c81a55436c2b39e1f9eeb63.jpg",
                                  "xtra_small": "http://image.femaledaily.com/dyn/160/images/user-pics/fb8d1be39c81a55436c2b39e1f9eeb63.jpg",
                                  "large": "http://image.femaledaily.com/dyn/640/images/user-pics/fb8d1be39c81a55436c2b39e1f9eeb63.jpg"
                              }
                          },
                          "redirect": {
                              "talk_id": 1106,
                              "reply_talk_id": null
                          },
                          "created_at": "2019-02-22T11:25:42.049Z",
                          "updated_at": "2019-02-22T11:25:42.049Z"
                      }


}