export default {
  FONTS: {
    NOTOSERIF       : 'NotoSerif-Regular',
    NOTOSERIF_BOLD  : 'NotoSerif-Bold',
    MONTSERRAT      : 'Montserrat-Regular',
    MONTSERRAT_BOLD : 'Montserrat-Bold'
  },
  COLORS: {
    WHITE           : `#FFF;`,
    PINK            : `#DB284E;`,
    GREY            : `#9B9B9B;`,
    DARK_GREY       : `#4A4A4A;`,
    LIGHT_GREY_1    : `#FAFAFA;`,
    LIGHT_GREY_2    : `#E0E0E0;`,
    LIGHT_GREY_3    : `#D0D0D0;`
  },
  FOOTER: {
    SIZE_TITLE: '20px',
    SIZE_SUB_TITLE: '14px'
  },
  FOOTER_CUSTOM: `
    font-size: 14px;
    font-weight: bold;
  `,
  // color for class
  COLOR: {
    PINK: `color: #DB284E;`,
    RGREY: `color: #FAFAFA;`,
    LGREY1: `color: #E0E0E0;`,
    LGREY2: `color: #D0D0D0;`,
    GREY: `color: #9B9B9B;`,
    DARKGREY: `color: #4A4A4A;`,
    WHITE: `color: #FFF;`,
  },
  // color list
  PINK: `#DB284E;`,
  RGREY: `#FAFAFA;`,
  LGREY1: `#E0E0E0;`,
  LGREY2: `#D0D0D0;`,
  GREY: `#9B9B9B;`,
  DARKGREY: `#4A4A4A;`,
  WHITE: `#FFF;`,
  // list typo
  TYPO: {
    BIGHEAD: `
      font-size: 40px;
      font-family: Montserrat-Bold, Times, serif;
    `,
    HEAD1:`
      font-family: NotoSerif-Bold, Times, serif;
      font-size: 30px;
    `,
    HEAD2:`
      font-family: NotoSerif-Regular, Times, serif;
      font-size: 20px;
    `,
    ARTICLE:`
      font-family: NotoSerif-Regular, Times, serif;
      font-size: 24px;
    `,
    SM_ARTICLE:`
      font-family: NotoSerif-Regular, Times, serif;
      font-size: 18px;
    `,
    SM_PRODUCT:`
      font-family: NotoSerif-Bold, Times, serif;
      font-size: 18px;
    `,
    BIG_BODY:`
      font-family: Montserrat-Light, Times, serif;
      font-size: 24px;
    `,
    SM_BODY:`
      font-family: Montserrat-Light, Times, serif;
      font-size: 18px;
    `,
    CAPTION_TXT:`
      font-family: Montserrat-Bold, Times, serif;
      font-size: 24px;
    `,
    AUTHOR_CATEGORY:`
      font-family: Montserrat-Regular, Times, serif;
      font-size: 16px;
    `,
    ADDITIONAL_INFO:`
      font-family: Montserrat-Regular, Times, serif;
      font-size: 14px;
    `,
    MENU_TXT:`
      font-family: Montserrat-Bold, Times, serif;
      font-size: 14px;
      word-spacing: 1px;
    `,
    SUBMENU_TXT:`
      font-family: Montserrat-Light, Times, serif;
      font-size: 16px;
    `,
    BTN_TXT:`
      font-family: Montserrat-Bold, Times, serif;
      font-size: 14px;
    `,
    TIME_TOTAL_TXT:`
      font-family: Montserrat-Bold, Times, serif;
      font-size: 14px;
    `,
    DURATION_TXT:`
      font-family: Montserrat-Bold, Times, serif;
      font-size: 10px;
      word-spacing: 0.5px;
    `,
  },

}
