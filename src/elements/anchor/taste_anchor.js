import React from 'react'
import PropTypes from 'prop-types'

import Link from 'next/link'

const TasteAnchor = ({ element_id, text, margin, padding, color, url }) => (
  <Link href={url}>
    <a id={element_id||''} className="taste-anchor" >
      {text}
      <style>
        {`
          .taste-anchor {
            font-family: Montserrat;
            font-size: 14px;
            color: ${color||'#4a4a4a'};
            margin: ${margin||0};
            padding: ${padding||0};
            text-decoration: none;
          }
          .taste-anchor:hover {
            color: #db284e;
          }
        `}
      </style>
    </a>
  </Link>
)

TasteAnchor.propTypes = {
  text: PropTypes.string.isRequired
}

export default TasteAnchor