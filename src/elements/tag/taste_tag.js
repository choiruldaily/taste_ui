import React from 'react'
import PropTypes from 'prop-types'

import Vars from '../../consts/vars'

import TasteTagMobile from './taste_tag_mobile'

const TasteTag = ({ type, element_id, is_mobile, text, style }) => {
  const props = {
    class_name  : 'taste-tag', 
    element_id  : element_id,
    text        : text.length > 25? text.substring(0,25)+'..' : text|| '',
    style       : style || ''
  }

  if (is_mobile) {
    return (
      <TasteTagMobile
        { ...props }
      />
    )
  } else {
    return (
      <TasteTagMobile
        { ...props }
      />
    )
  }
}

TasteTag.propTypes = {
  text: PropTypes.string.isRequired
}

export default TasteTag