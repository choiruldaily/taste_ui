import React from 'react'
import PropTypes from 'prop-types'

import Vars from '../../consts/vars'

const TasteTagMobile = (props) => (
  <p id={props.element_id} className={props.class_name}>
    { props.text }
    <style>
      {`
        .${props.class_name} {
          font-family: Montserrat;
          font-size: 14px;
          font-weight: 500;
          font-style: normal;
          font-stretch: normal;
          line-height: 2;
          letter-spacing: normal;
          color: #000;
          height: 30px;
          margin: 5px;
          border-radius: 8px;
          box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.25);
          background-color: #fafafa;
          padding: 0 10px;
          text-transform: capitalize;
          ${props.style}
        }
      `}
    </style>
  </p>
)

TasteTagMobile.propTypes = {
   text : PropTypes.string.isRequired
}

export default TasteTagMobile