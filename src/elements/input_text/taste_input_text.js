import React from 'react'
import PropTypes from 'prop-types'

import Vars from '../../consts/vars'

import TasteInputTextMobile from './taste_input_text_mobile'

const TasteInputText = ({ type, element_id, is_mobile, value, placeholder, style, additional_class, on_change, on_key_up }) => {
  const props = {
    type        : type || Vars.TASTE_TEXT_TYPE.DEFAULT, 
    class_name  : 'taste-input-text-'+(type || Vars.TASTE_TEXT_TYPE.DEFAULT)+(additional_class?'-'+additional_class:''), 
    element_id  : element_id,
    value       : value || '',
    placeholder : placeholder || '',
    style       : style || '',

    on_change,
    on_key_up
  }

  if (is_mobile) {
    return (
      <TasteInputTextMobile
        { ...props }
      />
    )
  } else {
    return (
      <TasteInputTextMobile
        { ...props }
      />
    )
  }
}

TasteInputText.propTypes = {
  value: PropTypes.string.isRequired
}

export default TasteInputText