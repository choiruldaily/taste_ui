import React from 'react'
import PropTypes from 'prop-types'

import Vars from '../../consts/vars'

const TasteInputTextMobile = (props) => (
  <React.Fragment>
    <input className={ props.class_name } defaultValue={ props.value } placeholder={ props.placeholder } width="50" height="20" onChange={props.on_change} onKeyUp={props.on_key_up}/>
    <style>
      {`
        .${props.class_name} {
          font-family: Montserrat;
          color: #000;
          letter-spacing: 1px;
          margin: 0;
          padding: 5px 10px;
          border-radius: 4px;
          border: 1px solid #9b9b9b;
          font-size: 12px;
          font-weight: 500;
          outline: none;
          width: 100%;
          height: 32px;
          ${props.style}
        }
        .${props.class_name}::placeholder {
          color: #bdbdbd;
          font-weight: 300;
        }
      `}
    </style>
  </React.Fragment>
)

TasteInputTextMobile.propTypes = {
   value : PropTypes.string.isRequired
}

export default TasteInputTextMobile