import React from 'react'
import Link from 'next/link'

const TasteInfoIconMobile = (props) => {
  return (
    <div className='taste-info'>      
      { 
        props.icon?
          <span className={props.icon}/> 
        : 
          <span className='icon-ic_like'/>
      }

      <span className='taste-info-txt'> { props.info }</span>

      <style>
        {`
          .taste-info {
            margin: 0 5px;
            padding: 0;
            display: flex;
            justify-content: center;
            align-items: center;
          }
          .taste-info-txt {
            font-family: Montserrat;
            font-size: 12px;
            font-weight: 600;
            font-style: normal;
            color: #000;
            margin-left: 5px;

          }
        
        `}
      </style>
    </div>
  )
}

export default TasteInfoIconMobile