import React from 'react'
import PropTypes from 'prop-types'

import TasteInfoIconMobile from './taste_info_icon_mobile'

const TasteInfoIcon = ({ is_mobile=false, icon, info='info' }) => {
  const props = { 
    icon : icon, 
    info : info > 0? info : '-'
  }
  
  if (is_mobile) {
    return (
      <TasteInfoIconMobile
        { ...props }
      />
    )
  } else {
    return (
      <TasteInfoIconMobile
        { ...props }
      />
    )
  }
}

TasteInfoIcon.propTypes = {
  is_mobile   : PropTypes.bool.isRequired,
}

export default TasteInfoIcon