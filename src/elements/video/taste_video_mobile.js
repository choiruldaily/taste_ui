import React from 'react'

import Vars from '../../consts/vars'

const TasteVideoMobile = (props) => (
  props.type===Vars.TASTE_VIDEO_TYPE.CLICKABLE?
    <div className={props.class_name} onClick={()=>{props.on_click(props.url,Vars.MODAL_TYPE.VIDEO)}}>
      <span className='icon-ic_play ic-play'/>
      <video className={props.class_name+'-video'} preload="metadata" onLoad={props.on_load} controls={props.is_control}>
        <source src={`${props.url}#t=15`} type={props.type_video} />
      </video>

      <style>
        {`
          .${props.class_name} {
            width: 100%;
            height: 100%;
            margin: 10px 20px 0 0;
            background-color: #efefef;

            flex-shrink: 0;
            display: flex;
            position: relative;
            justify-content: center;
            align-items: center;
            ${props.style}
          }
          .ic-play {
            position: absolute;
            z-index: 1;
            font-size: 32px;
          }
          .${props.class_name+'-video'} {
            width: 100%;
            height: 100%;
          }
        `}
      </style>        
    </div>
  :
    <div className={props.class_name}>
      <video className={props.class_name+'-video'} preload="metadata" onLoad={props.on_load} controls={props.is_control}>
        <source src={`${props.url}#t=15`} type={props.type} />
      </video>

      {
        props.is_show_close?
          <span className={'icon-button_delete '+props.class_name+'-close'} onClick={()=>{props.on_close(props.url)}}>
            <span className="path1"></span><span className="path2"></span>
          </span>
        : null
      }
      
      <style>
        {`
          .${props.class_name} {
            width: 100%;
            height: 100%;
            margin: 10px 20px 0 0;
            background-color: #efefef;
            position: relative;
            ${props.style}
          }
          .${props.class_name+'-video'} {
            width: 100%;
            height: 100%;
          }
          .${props.class_name+'-close'} {
            position: absolute;
            right: 0;
            top: 0;
            float: right;
            width: 20px;
            height: 20px;
            text-align: center;
          }
        `}
      </style>        
    </div>
)

export default TasteVideoMobile