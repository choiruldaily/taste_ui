import React from 'react'

import TasteVideoMobile from './taste_video_mobile'

const TasteVideo = ({ is_mobile, url, type, type_video, is_show_control, is_show_close, class_name, element_id,  style, additional_class, on_load, on_close, on_click }) => {
  const props = {
    url             : url || 'http://vollrath.com/ClientCss/images/VollrathImages/No_Image_Available.jpg',
    type_video      : type_video || 'video/mp4', 
    is_show_control : is_show_control,
    is_show_close   : is_show_close,
    class_name      : 'taste-video'+(class_name && '-'+class_name || '')+(additional_class && '-'+additional_class || ''), 
    element_id      : element_id,
    style           : style || '',
    type,

    on_load,
    on_close,
    on_click
  }

  if (is_mobile) {
    return (
      <TasteVideoMobile
        { ...props }
      />
    )
  } else {
    return (
      <TasteVideoMobile
        { ...props }
      />
    )
  }
}

export default TasteVideo