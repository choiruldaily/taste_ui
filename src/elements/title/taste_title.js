import React from 'react'
import PropTypes from 'prop-types'

import Vars from '../../consts/vars'

import TasteTitleMobile from './taste_title_mobile'
import TasteTitleDesktop from './taste_title_desktop'

const TasteTitle = ({ type, element_id, is_mobile, text, style, additional_class }) => {
  const props = {
    type        : type || Vars.TASTE_TITLE_TYPE.SUB_TITLE, 
    class_name  : 'taste-title-'+(type || Vars.TASTE_TITLE_TYPE.SUB_TITLE)+(additional_class?'-'+additional_class:''), 
    element_id  : element_id,
    text        : text || '',
    style       : style || '',
  }
  
  if (is_mobile) {
    return (
      <TasteTitleMobile
        { ...props }
      />
    )
  } else {
    return (
      <TasteTitleMobile
        { ...props }
      />
    )
  }
}


TasteTitle.propTypes = {
  text: PropTypes.string.isRequired
}

export default TasteTitle