import React from 'react'
import PropTypes from 'prop-types'

const TasteTitleDesktop = (props) => (
  <React.Fragment>
    <p id={props.element_id} className={props.class_name}>
      { props.text }
      <style>
        {`
          .${props.class_name} {
            font-family: Montserrat;
            font-weight: 500;
            font-size: 14px;
            color: #4a4a4a;
            margin: 0;
            padding:0;
            ${props.style}
          }
        `}
      </style>
    </p>
  </React.Fragment>
)

TasteTitleDesktop.propTypes = {
  text: PropTypes.string.isRequired
}

export default TasteTitleDesktop