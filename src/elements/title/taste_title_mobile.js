import React from 'react'
import PropTypes from 'prop-types'

import Vars from '../../consts/vars'

const TasteTitleMobile = (props) => (
  props.type===Vars.TASTE_TITLE_TYPE.DEFAULT?
    <h2 id={props.element_id} className={props.class_name}>
      { props.text }
      <style>
        {`
          .${props.class_name} {
            font-family: Montserrat;
            font-size: 18px;
            font-weight: 800;
            color: #000;
            margin: 0;
            padding:0;
            ${props.style}
          }
        `}
      </style>
    </h2>
  :
  props.type===Vars.TASTE_TITLE_TYPE.MAIN_TITLE?
    <h1 id={props.element_id} className={props.class_name}>
      { props.text }
      <style>
        {`
          .${props.class_name} {
            font-family: Montserrat;
            font-size: 18px;
            font-weight: 800;
            line-height: 1.3;
            color: #000;
            letter-spacing: 1.3px;
            margin: 0;
            padding:0;
            ${props.style}
          }
        `}
      </style>
    </h1>
  :
  props.type===Vars.TASTE_TITLE_TYPE.SUB_TITLE?
    <h2 id={props.element_id} className={props.class_name}>
      { props.text }
      <style>
        {`
          .${props.class_name} {
            font-family: Montserrat;
            font-size: 16px;
            font-weight: 800;
            // line-height: 1.5;
            color: #000;
            letter-spacing: 1.1px;
            margin: 0;
            padding:0;
            ${props.style}
          }
        `}
      </style>
    </h2>
  :
  props.type===Vars.TASTE_TITLE_TYPE.CARD_TITLE?
    <h3 id={props.element_id} className={props.class_name}>
      { props.text }
      <style>
        {`
          .${props.class_name} {
            font-family: Montserrat;
            font-size: 16px;
            font-weight: 700;
            color: #000;
            letter-spacing: 1.1px;
            margin: 0;
            padding:0;
            ${props.style}
          }
        `}
      </style>
    </h3>
  :
  props.type===Vars.TASTE_TITLE_TYPE.NAVIGATION?
    <h2 id={props.element_id} className={props.class_name}>
      { props.text }
      <style>
        {`
          .${props.class_name} {
            font-family: Montserrat;
            font-size: 16px;
            font-weight: 700;
            line-height: 1.5;
            color: #000;
            letter-spacing: 1.1px;
            margin: 0;
            padding:0;
            ${props.style}
          }
        `}
      </style>
    </h2>
  :
  props.type===Vars.TASTE_TITLE_TYPE.NAVIGATION_SELECTED?
    <h2 id={props.element_id} className={props.class_name}>
      { props.text }
      <style>
        {`
          .${props.class_name} {
            font-family: Montserrat;
            font-size: 16px;
            font-weight: 700;
            line-height: 1.5;
            color: #db284e;
            letter-spacing: 1.1px;
            margin: 0;
            padding:0;
            ${props.style}
          }
        `}
      </style>
    </h2>
  :
    <h3 id={props.element_id} className={props.class_name}>
      { props.text }
      <style>
        {`
          .${props.class_name} {
            font-family: Montserrat;
            font-size: 16px;
            font-weight: 700;
            color: #000;
            letter-spacing: 1.1px;
            margin: 0;
            padding:0;
            ${props.style}
          }
        `}
      </style>
    </h3>
)

TasteTitleMobile.propTypes = {
  text: PropTypes.string.isRequired
}

export default TasteTitleMobile