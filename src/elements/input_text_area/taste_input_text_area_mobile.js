import React from 'react'
import PropTypes from 'prop-types'

import Vars from '../../consts/vars'

const TasteTextAreaMobile = (props) => (
  <React.Fragment>
    <textarea className={ props.class_name } defaultValue={ props.value } placeholder={ props.placeholder } rows="6" onChange={props.on_change} />
    <style>
      {`
        .${props.class_name} {
          font-family: Montserrat;
          font-size: 14px;
          font-weight: 500;
          color: #000;
          letter-spacing: 1px;
          margin: 0;
          padding:0;
          width: 100%;
          border: none;
          outline: none;
          resize: none;
          ${props.style}
        }
        .${props.class_name}::placeholder {
          color: #bdbdbd;
          font-family: Montserrat;
          font-size: 14px;
          font-weight: 300;
        }
      `}
    </style>
  </React.Fragment>
)

TasteTextAreaMobile.propTypes = {
   value : PropTypes.string.isRequired
}

export default TasteTextAreaMobile