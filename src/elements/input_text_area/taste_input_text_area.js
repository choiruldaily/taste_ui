import React from 'react'
import PropTypes from 'prop-types'

import Vars from '../../consts/vars'

import TasteInputTextAreaMobile from './taste_input_text_area_mobile'

const TasteInputTextArea = ({ type, element_id, is_mobile, value, placeholder, style, additional_class, on_change  }) => {
  const props = {
    type        : type || Vars.TASTE_TEXT_TYPE.DEFAULT, 
    class_name  : 'taste-input-text-area-'+(type || Vars.TASTE_TEXT_TYPE.DEFAULT)+(additional_class?'-'+additional_class:''), 
    element_id  : element_id,
    value       : value || '',
    placeholder : placeholder || '',
    style       : style || '',

    on_change
  }

  if (is_mobile) {
    return (
      <TasteInputTextAreaMobile
        { ...props }
      />
    )
  } else {
    return (
      <TasteInputTextAreaMobile
        { ...props }
      />
    )
  }
}

TasteInputTextArea.propTypes = {
  value: PropTypes.string.isRequired
}

export default TasteInputTextArea