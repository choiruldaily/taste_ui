import React from 'react'
import PropTypes from 'prop-types'

import Vars from '../../consts/vars'

import TasteInputFileMobile from './taste_input_file_mobile'

const TasteInputFile = ({ is_mobile, format, element_id, icon, style, additional_class, on_click, on_change  }) => {
  const props = {
    format      : format || '.jpg',
    element_id  : element_id,
    icon        : icon || 'icon-icon_camera ic-camera',
    style       : style || '',
    class_name  : 'taste-input-file-'+(additional_class && '-'+additional_class || ''), 

    on_click,
    on_change
  }

  if (is_mobile) {
    return (
      <TasteInputFileMobile
        { ...props }
      />
    )
  } else {
    return (
      <TasteInputFileMobile
        { ...props }
      />
    )
  }
}

TasteInputFile.propTypes = {
  value: PropTypes.string.isRequired
}

export default TasteInputFile