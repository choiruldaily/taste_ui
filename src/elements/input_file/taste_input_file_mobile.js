import React from 'react'

const TasteInputFileMobile = (props) => (
  <span className={ props.class_name }>
    <i className={`${props.icon} ${props.class_name+'-icon'}`}/>
    <input 
      type="file"
      accept={props.format}
      className={props.class_name+'-input-file'} 

      onClick={(e)=>{e.target.value=''}} 
      onChange={props.on_change}
    />
    <style>
      {`
        .${props.class_name} {
          display: flex;
          width: 25px;
          height: 25px;
          ${props.style}
        }
        .${props.class_name+'-icon'} {
          left: 0;
        }
        .${props.class_name+'-input-file'} {
          position: absolute;
          opacity: 0;
          width: 25px;
          height: 25px;
        }
        .${props.class_name}::placeholder {
          color: #bdbdbd;
          font-weight: 300;
        }
      `}
    </style>
  </span>
)

export default TasteInputFileMobile