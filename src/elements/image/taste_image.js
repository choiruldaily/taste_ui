import React from 'react'

import Vars from '../../consts/vars'

import TasteImageMobile from './taste_image_mobile'
import TasteImageDesktop from './taste_image_desktop'

const TasteImage = ({ type, element_id, is_mobile, text, url, url_video, style, additional_class, is_show_close, on_close, on_click }) => {
  const props = {
    type        : type || Vars.TASTE_IMAGE_TYPE.DEFAULT, 
    class_name  : 'taste-image-'+(type || Vars.TASTE_IMAGE_TYPE.DEFAULT)+(additional_class?'-'+additional_class:''), 
    element_id  : element_id,
    text        : text || '',
    url         : url || 'http://vollrath.com/ClientCss/images/VollrathImages/No_Image_Available.jpg',
    url_video   : url_video || 'https://www.youtube.com/watch?v=Okq-pjRcMKA&t=1250s',
    style       : style || '',
    is_show_close: is_show_close,

    on_close,
    on_click 
  }

  if (is_mobile) {
    return (
      <TasteImageMobile
        { ...props }
      />
    )
  } else {
    return (
      <TasteImageMobile
        { ...props }
      />
    )
  }
}

export default TasteImage