import React from 'react'

import Vars from '../../consts/vars'

const TasteImageMobile = (props) => (
  props.type===Vars.TASTE_IMAGE_TYPE.CIRCLE?
    <React.Fragment>
      <img id={props.element_id} className={props.class_name} src={props.url}/>

      {
        props.is_show_close?
          <span onClick={()=>{props.on_close(props.url)}}>close x</span>
        : null
      }
      
      <style>
        {`
          .${props.class_name} {
            width: 100%;
            height: 100%;
            border-radius: 50%;
            ${props.style}
          }
        `}
      </style>
    </React.Fragment>
  :
  props.type===Vars.TASTE_IMAGE_TYPE.TEXT?
    <div className={props.class_name}>
      <p className='total-hidden-image'>{props.text}+</p>
      <img id={props.element_id} className='image-hidden' src={props.url}/>

      {
        props.is_show_close?
          <span onClick={()=>{props.on_close(props.url)}}>close x</span>
        : null
      }

      <style>
        {`
          .image-hidden {
            width: 100%;
            height: 100%;
            ${props.style}
          }
          .total-hidden-image {
            position: absolute;
            margin-top: auto;
            font-family: Montserrat;
            font-size: 36px;
            font-weight: 600;
            font-style: normal;
            font-stretch: normal;
            line-height: normal;
            letter-spacing: normal;
            color: #e0e0e0;

          }
          .${props.class_name} {
            display: flex;
            justify-content: center;
            align-items: center;
          }
        `}
      </style>
    </div>
  :
  props.type===Vars.TASTE_IMAGE_TYPE.CLICKABLE?
    <div className={props.class_name} onClick={()=>{props.on_click(props.url,Vars.MODAL_TYPE.IMAGE)}}>
      <img id={props.element_id} className={props.class_name+'-image'} src={props.url}/>
    <style>
      {`
        .${props.class_name} {
          width: 100%;
          height: 100%;
          flex-shrink: 0;
          display: flex;
          margin: 10px 10px 0 0;
          background-color: #efefef;
          position: relative;
          ${props.style}
        }
        .${props.class_name+'-image'} {
          width: 100%;
        }
        .${props.class_name+'-close'} {
          position: absolute;
          right: 0;
          top: 0;
          float: right;
          width: 20px;
          height: 20px;
          text-align: center;
        }
      `}
    </style>
  </div>
  :
  props.type===Vars.TASTE_IMAGE_TYPE.CLICK_VIDEO?
    <div className={props.class_name} onClick={()=>{props.on_click(props.url_video,Vars.MODAL_TYPE.VIDEO)}}>
      <span className='icon-ic_play ic-play'/>
      <img id={props.element_id} className={props.class_name+'-image'} src={props.url}/>
    <style>
      {`
        .${props.class_name} {
          width: 100%;
          height: 100%;
          flex-shrink: 0;
          display: flex;
          margin: 10px 10px 0 0;
          background-color: #efefef;
          position: relative;

          justify-content: center;
          align-items: center;
          ${props.style}
        }
        .${props.class_name+'-image'} {
          width: 100%;
        }
        .ic-play {
          position: absolute;
          font-size: 32px;
        }
        .${props.class_name+'-close'} {
          position: absolute;
          right: 0;
          top: 0;
          float: right;
          width: 20px;
          height: 20px;
          text-align: center;
        }
      `}
    </style>
  </div>
  :
  <div className={props.class_name}>
    <img id={props.element_id} className={props.class_name+'-image'} src={props.url}/>

    {
      props.is_show_close?
        <span className={'icon-button_delete '+props.class_name+'-close'} onClick={()=>{props.on_close(props.url)}}>
          <span className="path1"></span><span className="path2"></span>
        </span>
      : null
    }
    <style>
      {`
        .${props.class_name} {
          width: 100%;
          height: 100%;
          flex-shrink: 0;
          display: flex;
          margin: 10px 10px 0 0;
          background-color: #efefef;
          position: relative;
          ${props.style}
        }
        .${props.class_name+'-image'} {
          width: 100%;
        }
        .${props.class_name+'-close'} {
          position: absolute;
          right: 0;
          top: 0;
          float: right;
          width: 20px;
          height: 20px;
          text-align: center;
        }
      `}
    </style>
  </div>
)

export default TasteImageMobile