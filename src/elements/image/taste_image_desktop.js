import React from 'react'
import PropTypes from 'prop-types'

import Vars from '../../consts/vars'

const TasteImageDesktop = (props) => (
  <React.Fragment>
    {
      props.type===Vars.TASTE_IMAGE_TYPE.CIRCLE?
        <React.Fragment>
          <img id={props.element_id} className={props.class_name} src={props.url}/>
            <style>
              {`
                .${props.class_name} {
                  margin: 10px;
                  width: auto;
                  height: auto;
                  border-radius: 50%;
                  ${props.style}
                }
              `}
            </style>
        </React.Fragment>
      :
        <React.Fragment>
          <img id={props.element_id} className={props.class_name} src={props.url}/>
            <style>
              {`
                .${props.class_name} {
                  margin: 10px;
                  width: auto;
                  height: auto;
                  ${props.style}
                }
              `}
            </style>
        </React.Fragment>
    }
    
  </React.Fragment>
)

export default TasteImageDesktop