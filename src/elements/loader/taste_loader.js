import React from 'react'
import PropTypes from 'prop-types'

const TasteLoader = ({ text, image_host='/assets/images/'  }) => (
      <div className="modal-loader-outer">
        <div className="modal-loader-back"/>
        <div className="modal-loader">
          <div className="image-loader"/>
        </div>

        <style>
          {`
            .modal-loader-outer {
              width: 100%;
              height: 300px;
            }
            .modal-loader-back {
              position: fixed;
              top:0;
              width: 100%;
              height: 100%;
              background-color: #fff;
              opacity: 0.5;
              z-index: 2;
            }
            .modal-loader {
              width: 60px;
              height: 60px;
              min-height:30%;
              max-height:30%;
              top:30%;
              position: fixed;
              margin-left: auto;
              margin-right: auto;
              left: 0;
              right: 0;
              z-index: 2;
            }
            .image-loader {
              background: url(${image_host}ic_loader_breath_800.gif);
              background-repeat: no-repeat;
              background-size: 60px;
              width: 60px;
              height: 60px;
              margin: 0 auto;
              opacity: 1;
              background-color: #fff;
              border-radius: 50%;
            }
          `}
        </style>
      </div>
)

TasteLoader.propTypes = {
  is_mobile: PropTypes.bool.isRequired,
  text: PropTypes.string.isRequired
}

export default TasteLoader