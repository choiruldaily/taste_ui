import React from 'react'
import PropTypes from 'prop-types'

import TasteBreadcrumbMobile from './taste_breadcrumb_mobile'

const TasteBreadcrumb = ({ is_mobile=false, breadcrumbs }) => {
  const props = { breadcrumbs }
  
  if (is_mobile) {
    return (
      <TasteBreadcrumbMobile
        { ...props }
      />
    )
  } else {
    return (<div>Breadcrumb Desktop</div>)
  }
}

TasteBreadcrumb.propTypes = {
  is_mobile   : PropTypes.bool.isRequired,
  breadcrumbs : PropTypes.array.isRequired
}

export default TasteBreadcrumb