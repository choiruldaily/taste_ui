import React from 'react'
import PropTypes from 'prop-types'
import Link from 'next/link'

const TasteBreadcrumbMobile = ({ breadcrumbs=[] }) => (
  
    <ul className="breadcrumb">
      {
        breadcrumbs?
          breadcrumbs.map((item,idx)=>
            <li key={idx} className="breadcrumb-section">
              {
                idx<breadcrumbs.length-1?
                  <React.Fragment>
                    <Link 
                      href={{
                        pathname  : item.pathname,
                        query     : item.query
                      }} 
                      as={item.url}
                    >
                      <a className="breadcrumb-text">{item.title}</a>
                    </Link>
                    <i className="icon-btn_arrow_right_enabled ic-arrow"/>
                  </React.Fragment>
                :
                  <a className="breadcrumb-text-active">{item.title}</a>
              }
            </li>
          )
        : null
      }

      <style>
        {`
          .breadcrumb {
            width: 100%;
            padding: 10px;
            margin: 0;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
          }
          .ic-arrow {
            font-size: 8px;
            margin: 0 5px;
          }
          .breadcrumb-section {
            display: flex;
            flex-flow: row;
            align-items: center;
          }
          .breadcrumb-text {
            font-family: Montserrat;
            font-size: 11px;
            color: #DB284E;
          }
          .breadcrumb-text-active{
            font-family: Montserrat;
            font-size: 11px;
            margin: 0;
            color: #4A4A4A;
          }
          .breadcrumb li {
            display: inline;
          }
        `}
      </style>
    </ul>
)

TasteBreadcrumbMobile.propTypes = {
  breadcrumbs: PropTypes.string.isRequired
}

export default TasteBreadcrumbMobile