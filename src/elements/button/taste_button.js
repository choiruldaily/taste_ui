import React from 'react'

import Vars from '../../consts/vars'

import TasteButtonMobile from './taste_button_mobile'
import TasteButtonDesktop from './taste_button_desktop'

const TasteButton = ({ 
  type, 
  class_name, 
  class_icon, 
  element_id, 
  is_mobile, 
  button_type, 
  text, 
  style, 
  style_icon, 
  additional_class,
  is_disabled,

  on_click }) => {
  
    const props = {
    type        : type || Vars.TASTE_BUTTON_TYPE.DEFAULT,
    button_type : button_type||'button',
    class_name  : 'taste-button-'+(type || Vars.TASTE_BUTTON_TYPE.DEFAULT)+(additional_class?'-'+additional_class:''), 
    element_id  : element_id || null,
    style       : style || '',
    style_icon  : style_icon || '',
    text        : text || 'button',
    class_icon  : class_icon,
    is_disabled,
    
    on_click
  }
  
  if (is_mobile) {
    return (
      <TasteButtonMobile
        { ...props }
      />
    )
  } else {
    return (
      <TasteButtonMobile
        { ...props }
      />
    )
  }
}

export default TasteButton