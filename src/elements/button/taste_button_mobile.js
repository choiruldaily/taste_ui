import React from 'react'
import PropTypes from 'prop-types';

import Vars from '../../consts/vars'

const TasteButtonMobile = (props) => (
  props.is_disabled?
    <button id={props.element_id} className={props.class_name} type={props.button_type} disabled={props.is_disabled}>
      {props.class_icon?<span className={props.class_icon+' ic'}/>:null}{props.text}
      <style>
        {`
          .ic {
            margin: 2px 10px 0 0;
            ${props.style_icon}
          }
          .${props.class_name} {
            font-family: Montserrat;
            font-size: 14px;
            color: #eee;
            background-color: #ccc;
            margin: 0;
            padding: 10px;
            border: 1px solid #aaa;
            text-align: center;
            text-decoration: none;
            display: inline-flex;
            border-radius: 4px;
            outline: none;
            ${props.style}
          }
        `}
      </style>
    </button>
  : props.type===Vars.TASTE_BUTTON_TYPE.WHITE?
    <button id={props.element_id} className={props.class_name} type={props.button_type} onClick={()=>{props.on_click()}} disabled={props.is_disabled}>
        {props.class_icon?<span className={props.class_icon+' ic-white'}/>:null}{props.text}
        <style>
          {`
            .ic-white {
              margin: 2px 10px 0 0;
              ${props.style_icon}
            }
            .${props.class_name} {
              font-family: Montserrat;
              font-weight: 700;
              font-size: 14px;
              color: #db284e;
              background-color: #fff;
              margin: 0;
              padding: 12px;
              cursor: pointer;
              border: 1px solid #db284e;
              text-align: center;
              text-decoration: none;
              display: inline-flex;
              outline: none;
              ${props.style}
            }
          `}
        </style>
      </button>
    :
    props.type===Vars.TASTE_BUTTON_TYPE.WHITE_ROUND?
      <button id={props.element_id} className={props.class_name} type={props.button_type} onClick={()=>{props.on_click()}} disabled={props.is_disabled}>
        {props.class_icon?<span className={props.class_icon+' ic-white-round'}/>:null}{props.text}
        <style>
          {`
            .ic-white-round {
              margin: 2px 10px 0 0;
              ${props.style_icon}
            }
            .${props.class_name} {
              font-family: Montserrat;
              font-weight: 700;
              font-size: 14px;
              color: #db284e;
              background-color: #fff;
              margin: 0;
              padding: 12px;
              cursor: pointer;
              border: 1px solid #db284e;
              border-radius: 4px;
              text-align: center;
              text-decoration: none;
              display: inline-flex;
              outline: none;
              ${props.style}
            }
          `}
        </style>
      </button>
    :
    props.type===Vars.TASTE_BUTTON_TYPE.PINK?
      <button id={props.element_id} className={props.class_name} type={props.button_type} onClick={()=>{props.on_click()}} disabled={props.is_disabled}>
        {props.class_icon?<span className={props.class_icon+' ic-pink'}/>:null}{props.text}
        <style>
          {`
            .ic-pink {
              margin: 2px 10px 0 0;
              ${props.style_icon}
            }
            .${props.class_name} {
              font-family: Montserrat;
              font-weight: 700;
              font-size: 14px;
              color: #fff;
              background-color: #db284e;
              margin: 0;
              padding: 12px;
              cursor: pointer;
              border: 1px solid #db284e;
              text-align: center;
              text-decoration: none;
              display: inline-flex;
              outline: none;
              ${props.style}
            }
          `}
        </style>
      </button>
    :
    props.type===Vars.TASTE_BUTTON_TYPE.PINK_ROUND?
      <button id={props.element_id} className={props.class_name} type={props.button_type} onClick={()=>{props.on_click()}} disabled={props.is_disabled}>
        {props.class_icon?<span className={props.class_icon+' ic-pink-round'}/>:null}{props.text}
        <style>
          {`
            .ic-pink-round {
              margin: 2px 10px 0 0;
              ${props.style_icon}
            }
            .${props.class_name} {
              font-family: Montserrat;
              font-weight: 700;
              font-size: 14px;
              color: #fff;
              background-color: #db284e;
              margin: 0;
              padding: 12px;
              cursor: pointer;
              border: 1px solid #db284e;
              border-radius: 4px;
              text-align: center;
              text-decoration: none;
              display: inline-flex;
              outline: none;
              ${props.style}
            }
          `}
        </style>
      </button>
    :
      <button id={props.element_id} className={props.class_name} type={props.button_type} onClick={()=>{props.on_click()}} disabled={props.is_disabled}>
        {props.class_icon?<span className={props.class_icon+' ic'}/>:null}{props.text}
        <style>
          {`
            .ic {
              margin: 2px 10px 0 0;
              ${props.style_icon}
            }
            .${props.class_name} {
              font-family: Montserrat;
              font-size: 14px;
              color: #4a4a4a;
              background-color: #fff;
              margin: 0;
              padding: 10px;
              cursor: pointer;
              border: 1px solid #e0e0e0;
              text-align: center;
              text-decoration: none;
              display: inline-flex;
              border-radius: 4px;
              outline: none;
              ${props.style}
            }
          `}
        </style>
      </button>

)

TasteButtonMobile.propTypes = {
  text: PropTypes.string.isRequired
}

export default TasteButtonMobile