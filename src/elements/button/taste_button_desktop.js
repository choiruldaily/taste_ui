import React from 'react'
import PropTypes from 'prop-types'

const TasteButtonDesktop = ({ element_id, class_name ,style, type, button_type, text }) => (
  <button id={element_id} className={class_name} type={button_type}>
    {text}
    <style>
      {`
        .${class_name} {
          font-family: Montserrat;
          font-size: 14px;
          color: #4a4a4a;
          background-color: #fff;
          margin: 0;
          padding: 10px;
          cursor: pointer;
          border: 1px solid #e0e0e0;
          text-align: center;
          text-decoration: none;
          display: inline-block;
          border-radius: 4px;
          outline: none;
          ${style}
        }
      `}
    </style>
  </button>
)

TasteButtonDesktop.propTypes = {
  text: PropTypes.string.isRequired
}

export default TasteButtonDesktop