import React from 'react'
import PropTypes from 'prop-types'

import Vars from '../../consts/vars'

import TasteTextMobile from './taste_text_mobile'
import TasteTextDesktop from './taste_text_desktop'

const TasteText = ({ type, element_id, is_mobile, text, style, additional_class}) => {
  const props = {
    type        : type || Vars.TASTE_TEXT_TYPE.DEFAULT, 
    class_name  : 'taste-text-'+(type || Vars.TASTE_TEXT_TYPE.DEFAULT)+(additional_class?'-'+additional_class:''), 
    element_id  : element_id,
    text        : text || '',
    style       : style || ''
  }

  if (is_mobile) {
    return (
      <TasteTextMobile
        { ...props }
      />
    )
  } else {
    return (
      <TasteTextDesktop
        { ...props }
      />
    )
  }
}

TasteText.propTypes = {
  text: PropTypes.string.isRequired
}

export default TasteText