import React from 'react'
import PropTypes from 'prop-types'

import Vars from '../../consts/vars'

const TasteTextDesktop = (props) => (
  props.type===Vars.TASTE_TEXT_TYPE.DEFAULT?
    <p id={props.element_id} className={props.class_name}>
      { props.text }
      <style>
        {`
          .${props.class_name} {
            font-family: Montserrat;
            font-size: 14px;
            font-weight: 500;
            color: #000;
            margin: 0;
            padding:0;
            ${props.style}
          }
        `}
      </style>
    </p>
  :
  props.type===Vars.TASTE_TEXT_TYPE.DEFAULT_SPACING?
    <p id={props.element_id} className={props.class_name}>
      { props.text }
      <style>
        {`
          .${props.class_name} {
            font-family: Montserrat;
            font-size: 14px;
            font-weight: 500;
            color: #000;
            letter-spacing: 1px;
            margin: 0;
            padding:0;
            ${props.style}
          }
        `}
      </style>
    </p>
  :
  props.type===Vars.TASTE_TEXT_TYPE.DEFAULT_LIGHT?
    <p id={props.element_id} className={props.class_name}>
      { props.text }
      <style>
        {`
          .${props.class_name} {
            font-family: Montserrat;
            font-size: 14px;
            font-weight: 300;
            color: #000;
            margin: 0;
            padding:0;
            ${props.style}
          }
        `}
      </style>
    </p>
  :
  props.type===Vars.TASTE_TEXT_TYPE.DETAIL_NAME?
    <p id={props.element_id} className={props.class_name}>
      { props.text }
      <style>
        {`
          .${props.class_name} {
            font-family: Montserrat;
            font-weight: 500;
            font-size: 14px;
            color: #000;
            margin: 0;
            padding:0;
            ${props.style}
          }
        `}
      </style>
    </p>
  :
  props.type===Vars.TASTE_TEXT_TYPE.DETAIL_INFO?
    <p id={props.element_id} className={props.class_name}>
      { props.text }
      <style>
        {`
          .${props.class_name} {
            font-family: Montserrat;
            font-weight: 500;
            font-size: 12px;
            color: #9b9b9b;
            margin: 0;
            padding:0;
            ${props.style}
          }
        `}
      </style>
    </p>
  :
  props.type===Vars.TASTE_TEXT_TYPE.TIME?
    <p id={props.element_id} className={props.class_name}>
      { props.text }
      <style>
        {`
          .${props.class_name} {
            font-family: Montserrat;
            font-weight: 500;
            font-size: 12px;
            color: #c3c3c3;
            margin: 0;
            padding:0;
            ${props.style}
          }
        `}
      </style>
    </p>
  :
  props.type===Vars.TASTE_TEXT_TYPE.MENTION?
    <p id={props.element_id} className={props.class_name}>
      { props.text }
      <style>
        {`
          .${props.class_name} {
            font-family: Montserrat;
            font-weight: 700;
            font-size: 14px;
            color: #db284e;
            margin: 0;
            padding:0;
            ${props.style}
          }
        `}
      </style>
    </p>
  :
  props.type===Vars.TASTE_TEXT_TYPE.INFOBAR_TOPIC?
    <p id={props.element_id} className={props.class_name}>
      { props.text }
      <style>
        {`
          .${props.class_name} {
            font-family: Montserrat;
            font-weight: 600;
            font-size: 14px;
            letter-spacing: 1px;
            color: #000;
            margin: 0;
            padding:0;
            ${props.style}
          }
        `}
      </style>
    </p>
  :
  props.type===Vars.TASTE_TEXT_TYPE.INFOBAR_POST?
    <p id={props.element_id} className={props.class_name}>
      { props.text }
      <style>
        {`
          .${props.class_name} {
            font-family: Montserrat;
            font-weight: 600;
            font-size: 14px;
            letter-spacing: 1px;
            color: #9b9b9b;
            margin: 0;
            padding:0;
            ${props.style}
          }
        `}
      </style>
    </p>
  :
  props.type===Vars.TASTE_TEXT_TYPE.SORT?
    <p id={props.element_id} className={props.class_name}>
      { props.text }
      <style>
        {`
          .${props.class_name} {
            font-family: Montserrat;
            font-weight: 600;
            font-size: 14px;
            color: #000;
            margin: 0;
            padding:0;
            ${props.style}
          }
        `}
      </style>
    </p>
  :
  props.type===Vars.TASTE_TEXT_TYPE.PLACEHOLDER?
    <p id={props.element_id} className={props.class_name}>
      { props.text }
      <style>
        {`
          .${props.class_name} {
            font-family: Montserrat;
            font-weight: 500;
            font-size: 12px;
            color: #9b9b9b;
            margin: 0;
            padding:0;
            ${props.style}
          }
        `}
      </style>
    </p>
  :
    <p id={props.element_id} className={props.class_name}>
      { props.text }
      <style>
        {`
          .${props.class_name} {
            font-family: Montserrat;
            font-weight: 500;
            font-size: 14px;
            color: #4a4a4a;
            margin: 0;
            padding:0;
            ${props.style}
          }
        `}
      </style>
    </p>
)

TasteTextDesktop.propTypes = {
   text : PropTypes.string.isRequired
}

export default TasteTextDesktop