// Elements
import TasteLoader from './elements/loader/taste_loader'
import TasteButton from './elements/button/taste_button'
import TasteImage from './elements/image/taste_image'
import TasteVideo from './elements/video/taste_video'
import TasteText from './elements/text/taste_text'
import TasteTitle from './elements/title/taste_title'
import TasteBreadcrumb from './elements/breadcrumb/taste_breadcrumb'
import TasteTag from './elements/tag/taste_tag'
// import TasteIcon from './elements/icon/taste_icon'

// Form
import TasteInputText from './elements/input_text/taste_input_text'
import TasteInputTextArea from './elements/input_text_area/taste_input_text_area'
import TasteInputFile from './elements/input_file/taste_input_file'

// Components
import TasteHeader from './components/header/taste_header'
import TasteFooter from './components/footer/taste_footer'
import TasteMainMenu from './components/main_menu/taste_main_menu'
import TasteGroupCard from './components/group_card/taste_group_card'
import TasteUserCard from './components/user_card/taste_user_card'
import TasteTopicCard from './components/topic_card/taste_topic_card'
import TasteGroupDetail from './components/group_detail/taste_group_detail'
import TasteTopicDetail from './components/topic_detail/taste_topic_detail'
import TasteTalkCard from './components/talk_card/taste_talk_card'
import TastePagination from './components/pagination/taste_pagination'
import TasteNotificationCard from './components/notification_card/taste_notification_card'

module.exports = {
  TasteLoader,
  TasteButton,
  TasteImage,
  TasteVideo,
  TasteText,
  TasteTitle,
  TasteBreadcrumb,
  TasteTag,
  // TasteIcon,

  // Form
  TasteInputText,
  TasteInputTextArea,
  TasteInputFile,
  
  // Components
  TasteHeader,
  TasteFooter,
  TasteMainMenu,
  TasteGroupCard,
  TasteTopicCard,
  TasteUserCard,
  TasteGroupDetail,
  TasteTopicDetail,
  TasteTalkCard,
  TastePagination,
  TasteNotificationCard
}